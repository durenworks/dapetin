import 'package:dapetinapp/localdb/crud.dart';
import 'package:dapetinapp/pages/data/InputAdmin.dart';
import 'package:dapetinapp/pages/data/InputAm.dart';
import 'package:dapetinapp/pages/data/InputLayanan.dart';
import 'package:dapetinapp/pages/data/InputUser.dart';
import 'package:dapetinapp/pages/database/dataAdmin.dart';
import 'package:dapetinapp/pages/database/dataAm.dart';
import 'package:dapetinapp/pages/database/dataLayanan.dart';
import 'package:dapetinapp/pages/database/dataUser.dart';
import 'package:dapetinapp/pages/forgetPassword.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'blocs/baseBloc.dart';
import 'pages/dashboard.dart';
import 'pages/login/login.dart';

class GenProvider {
  static var providers = [
    ChangeNotifierProvider<BaseBloc>.value(value: BaseBloc(DBLogic()))
  ];

  static routes(context) {
    return {
      '/': (context) {
        return Dashboard();
      },

      '/login': (context) {
        return Login();
      },

      '/dashboard': (context) {
        return Dashboard();
      },
//      '/inputLayanan': (context) {
//        return InputLayanan();
//      },
      '/dashboard': (context) {
        return Dashboard();
      },
      '/inputAdmin': (context) {
        //return InputAdmin();
      },
//      '/inputUser': (context) {
//        return InputUser();
//      },
      '/inputAm': (context) {
        //return InputAm();
      },
//      '/dataLayanan': (context) {
//        return DataLayanan();
//      },
      '/dataAm': (context) {
        //return DataAm();
      },
      '/dataAdmin': (context) {
        //return DataAdmin();
      },
//      '/dataUser': (context) {
//        return DataUser();
//      },
      '/forget_password': (context) {
        return ForgetPassword();
      },
      '/': (context) {
        return Dashboard();
      },
    };
  }
}
