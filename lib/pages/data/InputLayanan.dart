import 'package:dapetinapp/blocs/baseBloc.dart';
import 'package:dapetinapp/common/GenColor.dart';
import 'package:dapetinapp/common/GenDimen.dart';
import 'package:dapetinapp/component/element/GenButton.dart';
import 'package:dapetinapp/component/element/genDropDownMap.dart';
import 'package:dapetinapp/component/element/genTextFormField.dart';
import 'package:dapetinapp/component/element/genTextShimmer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InputLayanan extends StatefulWidget {
  final TabController tabController;

  InputLayanan(this.tabController);

  @override
  _InputLayananState createState() => _InputLayananState();
}

class _InputLayananState extends State<InputLayanan> {
  var bloc;
  var jenisLayanan;
  var layanan;

  List listLayananKosong = [
    {"id": "1", "name": "Silahkan pilih jenis layanan"},
  ];

  void gotoData() {
    widget.tabController.animateTo(6);
  }

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<BaseBloc>(context);
    bloc.dashboard.getListServiceType();

    jenisLayanan != null
        ? bloc.dashboard.getListService(jenisLayanan)
        : bloc.dashboard.getListService("1");

    return Container(
      padding: EdgeInsets.all(GenDimen.paddingUtama),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Input Layanan",
            style: TextStyle(fontSize: GenDimen.fontTitle),
          ),
          SizedBox(
            height: GenDimen.paddingSub,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.all(GenDimen.paddingSub),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5), color: Colors.white),
              child: ListView(
                children: [
                  Text(
                    'INPUT DATA LAYANAN',
                    style: TextStyle(fontSize: GenDimen.fontSubTitle),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  StreamBuilder<Object>(
                      stream: bloc.dashboard.listServiceType,
                      builder: (BuildContext context,
                          AsyncSnapshot<dynamic> snapShot) {
                        if (!snapShot.hasData) {
                          return GenTextShimmer("LAYANAN");
                        }
                        return GenDropDownMap(
                          "LAYANAN",
                          snapShot.data,
                          jenisLayanan,
                          (val) {
                            setState(() {
                              jenisLayanan = val;
                              layanan = null;
                            });
                          },
                          id: "id",
                          val: "name",
                        );
                      }),

                  SizedBox(
                    height: GenDimen.jarakTextField,
                  ),

                  StreamBuilder<Object>(
                      stream: bloc.dashboard.listService,
                      builder: (BuildContext context,
                          AsyncSnapshot<dynamic> snapShot) {
                        if (!snapShot.hasData) {
                          return GenDropDownMap(
                            "LAYANAN",
                            listLayananKosong,
                            layanan,
                            (val) {},
                            id: "id",
                            val: "name",
                          );
                        } else {
                          print("jenis layanan " + jenisLayanan.toString());
                          return GenDropDownMap(
                            "LAYANAN",
                            jenisLayanan != null
                                ? snapShot.data
                                : listLayananKosong,
                            layanan,
                            (val) {
                              setState(() {
                                layanan = val;
                              });
                            },
                            id: "id",
                            val: "name",
                          );
                        }
                      }),
//        bloc.dashboard.getListService();
                  SizedBox(
                    height: GenDimen.jarakTextField,
                  ),
                  GenTextFromField("PAKET"),
                  SizedBox(
                    height: GenDimen.jarakTextField,
                  ),
                  GenTextFromField("MITRA"),
                  SizedBox(
                    height: GenDimen.jarakTextField,
                  ),
                  SizedBox(
                    height: GenDimen.jarakTextField,
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: GenButton(
                      "Save Input Layanan",
                      () {},
                      col: Colors.white,
                      bgCol: GenColor.red,
                      pad: GenDimen.paddingButtonBesar,
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
