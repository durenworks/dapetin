import 'dart:convert';

import 'package:dapetinapp/common/GenColor.dart';
import 'package:dapetinapp/common/GenDimen.dart';
import 'package:dapetinapp/component/element/genDropDown.dart';
import 'package:dapetinapp/component/element/genPreferrence.dart';
import 'package:dapetinapp/component/element/genShimmer.dart';
import 'package:dapetinapp/component/element/genTextFormField.dart';
import 'package:dapetinapp/models/User.dart';
import 'package:flutter/material.dart';
import 'package:dapetinapp/blocs/baseBloc.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:dapetinapp/api/request.dart';

class InputManajer extends StatefulWidget {
  final TabController tabController;

  InputManajer(this.tabController);

  @override
  _InputManajerState createState() => _InputManajerState();
}

class _InputManajerState extends State<InputManajer> {
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;
  var bloc;
  String segment = "Business Services";
  String witel = "SAMARINDA";
  int userId = 0;
  String nik;
  String email;
  String namaLengkap;
  String userTelegram;
  String userNameTelegram;
  String nohp;
  String password;
  bool is_edit = false;

  TextEditingController _controllerNik = TextEditingController();
  TextEditingController _controllerSegment = TextEditingController();
  TextEditingController _controllerName = TextEditingController();
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerTelegramId = TextEditingController();
  TextEditingController _controllerPhone = TextEditingController();
  TextEditingController _controllerTelegramName = TextEditingController();
  TextEditingController _controllerWitel = TextEditingController(); 
  TextEditingController _controllerPassword = TextEditingController(); 
  User user;
  
  List<String> _segmentList = [
    "Business Services",
    "Enterprise Services",
    "Government Services"
  ];

  List<String> _witelList = [
    "SAMARINDA",
    "KALTARA",
    "BALIKPAPAN",
    "KALBAR",
    "KALTENG",
    "KALSEL"
  ];

  void gotoData() {
    widget.tabController.animateTo(11);
  }

  Future<void> _fillForm() async{

      var data = await getPrefferenceUserObj();
      
      if (data != null) {
        var jsonData = json.decode(data);
      
        userId = jsonData["id"];
        segment = jsonData["segment"];
        _controllerNik.text = jsonData["nik"];
        _controllerEmail.text = jsonData["email"];
        _controllerName.text = jsonData["name"];
        _controllerTelegramId.text = jsonData["telegram_id"];
        _controllerPhone.text = jsonData["phone"];
        _controllerTelegramName.text = jsonData["telegram_name"];
        _controllerPassword.text = '';
        witel = jsonData["witel"];   
        is_edit = true;
        setPrefferenceUserObj(null);
      }
      
  }
  
  
  @override
  void initState() {  
    super.initState();
    _isLoading = false;
  }

  Future<void> _submit(context) async {
    final form = _formKey.currentState;

    if (form.validate()) {
      if (!is_edit) {
        form.save();
        setState(() => _isLoading = true);
        if( await postAddUser(segment, nik, namaLengkap, email,
              userTelegram,  nohp,  userNameTelegram,  witel,  password, "MANAGER")){
            //print("sukses");
            Toast.show("Input Berhasil!", context, duration: Toast.LENGTH_LONG);
            setState(() => _isLoading = false);
            form.reset();
            gotoData();
        }
        else {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          String errorMsg = prefs.getString('error_msg');
          Toast.show("Input Gagal : " + errorMsg, context, duration: Toast.LENGTH_LONG);
          setState(() => _isLoading = false);
        }
      }
      else {
        form.save();
        setState(() => _isLoading = true);
        if( await postEditUser(userId, segment, nik, namaLengkap, email,
              userTelegram,  nohp,  userNameTelegram,  witel,  password, "MANAGER")){
            //print("sukses");
            Toast.show("Update Berhasil!", context, duration: Toast.LENGTH_LONG);
            setState(() => _isLoading = false);
            form.reset();
            gotoData();
        }
        else {
          //SharedPreferences prefs = await SharedPreferences.getInstance();
          var errorMsg = await getPrefferenceErrorMsg();
          Toast.show("Update Gagal : " + errorMsg, context, duration: Toast.LENGTH_LONG);
          setState(() => _isLoading = false);
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<BaseBloc>(context);
    var media = MediaQuery.of(context);
    var size = media.size;
    if (getPrefferenceUserObj() != null) {
      _fillForm();
    }

    var inputData = Container(
        color: Colors.white,
        padding: EdgeInsets.only(left: 30, top: 30, right: 30),
        margin: EdgeInsets.only(top: 38, right: 50),
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(left: 30, right: 30),
            child: Column(children: [
              Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "INPUT DATA MANAGER",
                        style: TextStyle(fontSize: 18),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      GenDropDown(
                        "SEGMENT",
                        _segmentList,
                        segment,
                        (val) {
                          setState(() {
                            segment = val;
                          });
                        },
                        controller: _controllerSegment,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      GenTextFromField(
                        "NIK",
                        onSaved: (val) {
                          setState(() {
                            nik = val;
                          });
                        },
                        validator: (val) {
                          return val.length < 1
                              ? "NIK Tidak Boleh Kosong"
                              : null;
                        },
                        controller: _controllerNik,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      GenTextFromField(
                        "Email",
                        onSaved: (val) {
                          setState(() {
                            email = val;
                          });
                        },
                        validator: (val) {
                          return val.length < 1
                              ? "Email Tidak Boleh Kosong"
                              : null;
                        },
                        controller: _controllerEmail,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      GenTextFromField("NAMA LENGKAP", 
                        onSaved: (val) {
                          setState(() {
                            namaLengkap = val;
                          });
                        },
                        validator: (val) {
                          return val.length < 1
                              ? "Nama Tidak Boleh Kosong"
                              : null;
                        },
                        controller: _controllerName,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      GenTextFromField("Telegram ID", 
                        onSaved: (val) {
                          setState(() {
                            userTelegram = val;
                          });
                        },
                        validator: (val) {
                            return val.length < 1
                                ? "Telegram ID Tidak Boleh Kosong"
                                : null;
                        },
                        controller: _controllerTelegramId,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      GenTextFromField("NO. HP", onSaved: (val) {
                        setState(() {
                          nohp = val;
                        });
                      },
                      validator: (val) {
                          return val.length < 1
                              ? "No HP Tidak Boleh Kosong"
                              : null;
                        },
                        controller: _controllerPhone,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      GenTextFromField("USER NAME TELEGRAM", onSaved: (val) {
                        setState(() {
                          userNameTelegram = val;
                        });
                        },
                        validator: (val) {
                            return val.length < 1
                                ? "Username Telegram Tidak Boleh Kosong"
                                : null;
                        },
                        controller: _controllerTelegramName,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      GenDropDown("WITEL", _witelList, witel, (val) {
                        setState(() {
                          witel = val;
                        });
                        },
                        controller: _controllerWitel,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      GenTextFromField("PASSWORD", onSaved: (val) {
                        setState(() {
                          password = val;
                        });
                      },
                      validator: !is_edit ? (val) {
                          return val.length < 1
                              ? "Password Tidak Boleh Kosong"
                              : null;
                        } : null,
                        controller: _controllerPassword),
                      SizedBox(
                        height: 50,
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: RaisedButton(
                          color: GenColor.redButton,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          onPressed: () {
                            _submit(context);
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              "Save Input",
                              style: TextStyle(color: Colors.white, fontSize: 18),
                            ),
                          ),
                        ),
                      ), 
                      SizedBox(
                        height: 50,
                      ),
                    ]),
              )]
            )
          )
        )
    );

    return Container(
      padding: EdgeInsets.all(GenDimen.paddingUtama),
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              children: <Widget>[
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Container(
                    padding: EdgeInsets.only(top: 20),
                    child: Text(
                      "Input Data AM",
                      style:
                          TextStyle(fontWeight: FontWeight.w500, fontSize: 25),
                    ),
                  ),
                  inputData

                  // ],
                  //   ),
                  // ),
                ]),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
