import 'package:dapetinapp/component/element/GenButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class DataPage extends StatefulWidget {
  final TabController tabController;
  DataPage(this.tabController);

  @override
  _DataPageState createState() => _DataPageState();
}

class _DataPageState extends State<DataPage> {

  void goto(int to) {
    widget.tabController.animateTo(to);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(mainAxisAlignment: MainAxisAlignment.center,crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      //GenButton("input layanan",()=>goto(11)),
      GenButton("input Admin",()=>goto(8)),
      GenButton("input AM",()=>goto(5)),
      GenButton("input Manajer",()=>goto(12)),

    ],
      ),
    );
  }
}
