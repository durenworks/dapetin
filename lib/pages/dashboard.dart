import 'package:dapetinapp/common/GenColor.dart';
import 'package:dapetinapp/models/RecentActivity.dart';
import 'package:dapetinapp/pages/contract/ContractPage.dart';
import 'package:dapetinapp/pages/dashboard/dashboardContent.dart';
import 'package:dapetinapp/pages/data/DataPage.dart';
import 'package:dapetinapp/pages/data/InputAm.dart';
import 'package:dapetinapp/pages/database/dataAm.dart';
import 'package:dapetinapp/pages/database/dataLayanan.dart';
import 'package:dapetinapp/pages/database/databasePage.dart';
import 'package:dapetinapp/pages/profil/profilPage.dart';
import 'package:dapetinapp/pages/reminder/reminderPage.dart';
import 'package:flutter/material.dart';
import 'data/InputAdmin.dart';
import 'data/InputLayanan.dart';
import 'data/InputUser.dart';
import 'database/dataAdmin.dart';
import 'database/dataUser.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard>
    with SingleTickerProviderStateMixin {

  var title = "";
  final _formKey = GlobalKey<FormState>();
  var openBar = false;
  TabController tabController;

  Widget getItem({Icon icon, bool side, double height}) {
    return RotatedBox(
      quarterTurns: -1,
      child: side
          ? Container(
              height: height != null ? height : 50,
              width: 50,
              child: Center(
                child: icon,
              ),
            )
          : Container(height: 1,),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    
    tabController = TabController(initialIndex: 0, length: 13, vsync: this);
    tabController.addListener(() {
      switch (tabController.index) {
//        case 0:
//          {
//            setState(() {
//              title = "Profile";
//            });
//            break;
//          }
        case 1:
          {
            setState(() {
              title = "Home";
            });
            break;
          }
        case 2:
          {
            setState(() {
              title = "Input Contract";
            });
            break;
          }
        case 3:
          {
            setState(() {
              title = "Input Data";
            });
            break;
          }
        case 4:
          {
            setState(() {
              title = "Contract Reminder";
            });
            break;
          }
        case 5:
          {
            setState(() {
              title = "Database";
            });
            break;
          }
        case 6:
          {
            setState(() {
              title = "Input AM";
            });
            break;
          }
        case 7:
          {
            setState(() {
              title = "Data AM";
            });
            break;
          }
        case 8:
          {
            setState(() {
              title = "Data Admin";
            });
            break;
          }
        case 9:
          {
            setState(() {
              title = "Input Admin";
            });
            break;
          }
        case 10:
          {
            setState(() {
              title = "Data Layanan";
            });
            break;
          }
        case 11:
          {
            setState(() {
              title = "Input Layanan";
            });
            break;
          }
        case 12:
          {
            setState(() {
              title = "Data User";
            });
            break;
          }
        case 13:
          {
            setState(() {
              title = "Input User";
            });
            break;
          }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context);
    var size = media.size;
    var orientation = media.orientation;
    
    var Header = PreferredSize(
      preferredSize: Size.fromHeight(55), // here the desired height
      child: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(color: Colors.white),
          height: 55,
          child: Row(
            children: <Widget>[
              InkWell(
                onTap: () {
                  setState(() {
                    openBar = !openBar;
                  });
                },
                child: Image.asset('assets/images/menu-icon.png'),
              ),
              Container(width: 20),
              Image.asset(
                'assets/images/logo-dapetin.png',
                height: 25,
              ),
              Expanded(
                  child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[

                    Container(
                      height: 55,
                      width: 55,
                      child: Stack(
                        children: <Widget>[
                          Center(
                            child: InkWell(
                              onTap: () {
                                Navigator.pushNamed(context, "/notif");
                              },
                              child: Image.asset(
                                'assets/images/bell.png',
                                height: 25,
                              ),
                            ),
                          ),
                          Positioned(
                            top: 5,
                            left: 25,
                            child: Container(
                              padding: EdgeInsets.all(3),
                              margin: EdgeInsets.only(),
                              decoration: BoxDecoration(
                                  color: Colors.pink, shape: BoxShape.circle),
                              child: Text(
                                "0",
                                style: TextStyle(
                                    fontSize: 10,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, "/notif");
                      },
                      child: Image.asset(
                        'assets/images/login-simple-line-icons.png',
                        height: 25,
                      ),
                    ),
                  ],
                ),
              ))
            ],
          ),
        ),
      ),
    );
    var tabDashboard = Container(color: Colors.white,
        child: TabBar(
      isScrollable: true,
      indicatorPadding: EdgeInsets.all(1),
      controller: tabController,
      tabs: [
        // getItem(
        //     icon: Icon(
        //       Icons.account_circle,
        //       color: Colors.black,
        //     ),
        //     side: true,
        //     height: 100), 
        getItem(
            icon: Icon(
              Icons.home,
              color: Colors.black,
            ),
            side: true),
        getItem(
            icon: Icon(
              Icons.border_color,
              color: Colors.black,
            ),
            side: true),
        getItem(
            icon: Icon(
              Icons.assessment,
              color: Colors.black,
            ),
            side: true),
        getItem(
            icon: Icon(
              Icons.notifications,
              color: Colors.black,
            ),
            side: true),
        getItem(
            icon: Icon(
              Icons.data_usage,
              color: Colors.black,
            ),
            side: true),
        getItem(side: false),
        getItem(side: false),
        getItem(side: false),
        getItem(side: false),
        getItem(side: false),
        getItem(side: false),
        getItem(side: false),
        getItem(side: false),
      ],
    ));

    return Scaffold(
        appBar: Header,
        body: Stack(children: <Widget>[
          Row(children: [
            RotatedBox(
              quarterTurns: 1,
              child: tabDashboard,
            ),
            Expanded(
              child: TabBarView(
                physics: NeverScrollableScrollPhysics(),
                controller: tabController,
                children: [
//                  ProfilPage(),
                  DashBoardContent(tabController),
                  ContractPage(),
                  DataPage(tabController),
                  ReminderPage(),
                  DatabasePage(tabController),
                  InputAm(tabController),
                  DataAm(tabController),
                  DataAdmin(tabController),
                  InputAdmin(tabController),
                  DataLayanan(tabController),
                  InputLayanan(tabController),
                  DataManajer(tabController),
                  InputManajer(tabController),
                ],
              ),
//            openBar ? WideSidebar(size.width / 3) : SmallSidebar()
            )
          ])
        ]));
  }
}
