import 'dart:convert';

import 'package:dapetinapp/api/request.dart';
import 'package:dapetinapp/blocs/baseBloc.dart';
import 'package:dapetinapp/common/GenColor.dart';
import 'package:dapetinapp/component/element/genPreferrence.dart';
import 'package:dapetinapp/component/element/genRecentActivities.dart';
import 'package:dapetinapp/component/element/genShimmer.dart';
import 'package:dapetinapp/models/Kontrak.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class DashBoardContent extends StatefulWidget {
  final TabController tabController;

  DashBoardContent(this.tabController);

  @override
  _DashBoardContentState createState() => _DashBoardContentState();
}

class _DashBoardContentState extends State<DashBoardContent> {
  var bloc;

  void gotoEditKontrak() {
    widget.tabController.animateTo(1);
  }

  void gotoInputContract() {
    widget.tabController.animateTo(1);
  }

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context);
    var size = media.size;
    var orientation = media.orientation;

    var n = 1;
    bloc = Provider.of<BaseBloc>(context);
    bloc.dashboard.getStatistics();
    bloc.dashboard.getListRecentActivities();
    bloc.dashboard.getListReminders(
        "FBqCd2a2x7PVAjFbndYLgUwqPCmG3ajeLuhK04fagiRXu4zE8YPBsQIeitJXGs7Op4XdoBVxUCqdhoUc");

    var Statistics = Container(
      height: 125,
      child: StreamBuilder(
          stream: bloc.dashboard.statistics,
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapShot) {
            if (!snapShot.hasData) {
              return Container(
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 30, right: 15),
                      width: 253,
                      padding:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 30, right: 15),
                      width: 253,
                      padding:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 30, right: 15),
                      width: 253,
                      padding:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 30, right: 15),
                      width: 253,
                      padding:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                    ),
                  ],
                ),
              );
            } else {
              return Container(
                  child: orientation == Orientation.landscape
                      ? ListView(
                          scrollDirection: Axis.horizontal,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: 30, right: 15),
                              width: 253,
                              padding: EdgeInsets.symmetric(
                                  vertical: 20, horizontal: 10),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10))),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                      width: 60,
                                      height: 60,
                                      //margin: EdgeInsets.only(
                                      //          left:42,
                                      //          top:33,
                                      //          bottom:32),
                                      decoration: BoxDecoration(
                                          color: GenColor.hoverRed,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5))),
                                      child: Center(
                                          child: Icon(Icons.data_usage))),
                                  Container(
                                    width: 150,
                                    padding: EdgeInsets.only(left: 26),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          snapShot.data['totalContract']
                                              .toString(),
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 32),
                                        ),
                                        Text(
                                          "Data Contract ",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12,
                                              color: Colors.grey),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 30, right: 15),
                              width: 253,
                              padding: EdgeInsets.symmetric(
                                  vertical: 20, horizontal: 10),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10))),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                      width: 60,
                                      height: 60,
                                      //margin: EdgeInsets.only(
                                      //          left:42,
                                      //          top:33,
                                      //          bottom:32),
                                      decoration: BoxDecoration(
                                          color: GenColor.hoverGreen,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5))),
                                      child:
                                          Center(child: Icon(Icons.data_usage))
                                      //color: GenColor.red,
                                      ),
                                  Container(
                                    width: 150,
                                    padding: EdgeInsets.only(left: 26),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          snapShot.data['totalCustomer']
                                              .toString(),
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 32),
                                        ),
                                        Text(
                                          "Data Pelanggan ",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12,
                                              color: Colors.grey),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 30, right: 15),
                              width: 253,
                              padding: EdgeInsets.symmetric(
                                  vertical: 20, horizontal: 10),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10))),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: 60,
                                    height: 60,
                                    //margin: EdgeInsets.only(
                                    //          left:42,
                                    //          top:33,
                                    //          bottom:32),
                                    decoration: BoxDecoration(
                                        color: GenColor.hoverRed,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    child:
                                        Center(child: Icon(Icons.data_usage)),
                                  ),
                                  Container(
                                    width: 150,
                                    padding: EdgeInsets.only(left: 26),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          snapShot.data['totalService']
                                              .toString(),
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 32),
                                        ),
                                        Text(
                                          "Data Layanan ",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12,
                                              color: Colors.grey),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                gotoInputContract();
                              },
                              child: Container(
                                margin: EdgeInsets.only(top: 30, right: 15),
                                width: 291,
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 10),
                                decoration: BoxDecoration(
                                    color: GenColor.red,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.all(10),
                                      margin: EdgeInsets.only(right: 10),
                                      decoration: BoxDecoration(
                                          color: GenColor.hoverGreen,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5))),
                                      child: Image.asset(
                                          'assets/images/icn-plus.png'),
                                    ),
                                    Container(
                                      width: 118,
                                      child: Text(
                                        "INPUT CONTRACT",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w600,
                                            fontSize: 20),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                      : Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Expanded(
                                  child: Container(
                                    margin: EdgeInsets.all(10),
                                    padding: EdgeInsets.symmetric(vertical: 10),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10))),
                                    child: SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                              width: 40,
                                              height: 40,
                                              //margin: EdgeInsets.only(
                                              //          left:42,
                                              //          top:33,
                                              //          bottom:32),
                                              decoration: BoxDecoration(
                                                  color: GenColor.hoverRed,
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              child: Center(
                                                  child:
                                                      Icon(Icons.data_usage))),
                                          Container(
                                            padding: EdgeInsets.only(left: 10),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  snapShot.data['totalContract']
                                                      .toString(),
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 20),
                                                ),
                                                Text(
                                                  "Data Contract ",
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 12,
                                                      color: Colors.grey),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(width: 10),
                                Expanded(
                                  child: Container(
                                    margin: EdgeInsets.all(10),
                                    padding: EdgeInsets.symmetric(vertical: 10),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10))),
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                            width: 40,
                                            height: 40,
                                            //margin: EdgeInsets.only(
                                            //          left:42,
                                            //          top:33,
                                            //          bottom:32),
                                            decoration: BoxDecoration(
                                                color: GenColor.hoverGreen,
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5))),
                                            child: Center(
                                                child: Icon(Icons.data_usage))
                                            //color: GenColor.red,
                                            ),
                                        Container(
                                          padding: EdgeInsets.only(left: 10),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                snapShot.data['totalCustomer']
                                                    .toString(),
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 20),
                                              ),
                                              Text(
                                                "Data Pelanggan ",
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 12,
                                                    color: Colors.grey),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ));
            }
          }),
    );
    var reminderOfTheDay = StreamBuilder(
        stream: bloc.dashboard.listReminder,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapShot) {
          if (!snapShot.hasData) {
            return Container(
              padding: EdgeInsets.only(left: 10, top: 38, right: 30),
              margin: EdgeInsets.only(top: 38),
              width: 790,
              height: 396,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.black12),
            );
          } else {
//            print("snapshotnya" + snapShot.data['data'].toString());
            return  Container(
                color: Colors.white,
                padding: EdgeInsets.only(left: 15, top: 15, right: 15),
                margin: EdgeInsets.only(top: 38),
                height: 396,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Reminder Of The Day",
                        style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 15),
                      ),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: DataTable(
                            columnSpacing: 15,
                            sortColumnIndex: 1,
                            sortAscending: true,
                            columns: [
                              DataColumn(
                                label: ConstrainedBox(
                                  constraints: BoxConstraints(
                                    maxWidth: 50,
                                    minWidth: 50,
                                  ),
                                  child: Text('Segment'),
                                ),
                              ),
                              DataColumn(
                                onSort: (columnIndex, ascending) => true,
                                label: Text('Account Manager'),
                              ),
                              DataColumn(
                                label: ConstrainedBox(
                                  constraints: BoxConstraints(
                                    maxWidth: 100,
                                    minWidth: 50,
                                  ),
                                  child: Text('No. Kontrak'),
                                ),
                              ),
                              DataColumn(label: Text('Nama_Pelanggan')),
                              DataColumn(label: Text('Action')),
                              DataColumn(
                                label: ConstrainedBox(
                                  constraints: BoxConstraints(
                                    maxWidth: 50,
                                    minWidth: 50,
                                  ),
                                  child: Text(''),
                                ),
                              ),
                            ],
                            rows: snapShot.data['data'].map<DataRow>((e) {
                              var data = snapShot.data['data'];
                              var segment = data;
                              return DataRow(cells: [
//                        DataCell(Text((n++).toString())),
                                DataCell(
                                    Text(e["segment"]["acronym"].toString())),
                                DataCell(Text(e["user"]["name"].toString())),
                                DataCell(Text(e["contract_number"])),
                                DataCell(Text(e["customer_name"])),
                                DataCell(RaisedButton(
                                  color: Colors.redAccent,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  onPressed: () async {
                                    setPrefferenceEditKontrak(e['id']);
                                    gotoEditKontrak();
                                  },
                                  child: Text(
                                    "Amandemen",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                )),
                                DataCell(RaisedButton(
                                  color: Colors.redAccent,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  onPressed: () {},
                                  child: Text("Do",
                                      style: TextStyle(color: Colors.white)),
                                ))
                              ]);
                            }).toList()),
                      ),
                    ],
                  ),
                ),
              );
          }
        });
    var recentActivities = StreamBuilder(
        stream: bloc.dashboard.listRecentActivities,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapShot) {
          if (!snapShot.hasData) {
            return Container(
              margin: EdgeInsets.only(left: 15, top: 38),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.black12),
              width: 290,
              height: 396,
            );
          } else {
            return Container(
              padding: EdgeInsets.only(left: 15, top: 15, right: 15),
              margin: EdgeInsets.only(left: 15, top: 38),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10), color: Colors.white),
              width: 350,
              height: 396,
              child: ListView(
                children: [
                  Text(
                    "Recent Activities",
                    style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Column(
                    children: snapShot.data['data'].map<Widget>((e) {
                      return GenRecentActivities(e["activities"],
                          e["contract_number"], e["end_date_indo"]);
                    }).toList(),
                  )
                ],
              ),
            );
          }
        });

    return Container(
      padding: EdgeInsets.only(left: 10),
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              children: <Widget>[
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Container(
                    padding: EdgeInsets.only(top: 20),
                    child: Text(
                      "Welcome",
                      style:
                          TextStyle(fontWeight: FontWeight.w500, fontSize: 25),
                    ),
                  ),
                  Container(
                    child: Text(
                      "",
                      style:
                          TextStyle(fontWeight: FontWeight.w500, fontSize: 25),
                    ),
                  ),
                  Statistics,
                  Container(
                    width: size.width,
                    height: 396,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        reminderOfTheDay,
                        recentActivities,
                      ],
                    ),
                  ),
                  // ],
                  //   ),
                  // ),
                ]),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
