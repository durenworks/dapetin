import 'package:dapetinapp/blocs/baseBloc.dart';
import 'package:dapetinapp/common/GenColor.dart';
import 'package:dapetinapp/common/GenDimen.dart';
import 'package:dapetinapp/component/element/GenButton.dart';
import 'package:dapetinapp/component/element/genDropDown.dart';
import 'package:dapetinapp/component/element/genDropDownMap.dart';
import 'package:dapetinapp/component/element/genShimmer.dart';
import 'package:dapetinapp/component/element/genTextShimmer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ReminderPage extends StatefulWidget {
  @override
  _ReminderPageState createState() => _ReminderPageState();
}

class _ReminderPageState extends State<ReminderPage> {
  var bloc;
  var segment;
  var am;
  var status;

  List<String> listStatus = ["status A", "status B"];

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<BaseBloc>(context);
    bloc.dashboard.getListListSegment();
    bloc.dashboard.getListAm();
    bloc.dashboard.getListReminders("-");

    var reminderOfTheDay = StreamBuilder(
        stream: bloc.dashboard.listReminder,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapShot) {
          if (!snapShot.hasData) {
            return Container(
              child: GenShimmer(Container(
                padding: EdgeInsets.only(left: 10, top: 38, right: 30),
                margin: EdgeInsets.only(top: 38),
                width: 790,
                height: 396,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white),
              )),
            );
          } else {
            //print("snapshotnya" + snapShot.data['data'].toString());
            return Container(
              color: Colors.white,
              padding: EdgeInsets.only(left: 15, top: 15, right: 15),
              margin: EdgeInsets.only(top: 38),
              width: 790,
              height: 396,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Reminder Of The Day",
                    style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15),
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: DataTable(
                        columnSpacing: 15,
                        sortColumnIndex: 1,
                        sortAscending: true,
                        columns: [
//                    DataColumn(
//                      label: ConstrainedBox(
//                        constraints: BoxConstraints(
//                          maxWidth: 15,
//                          minWidth: 15,
//                        ),
//                        child: Text('No'),
//                      ),
//                    ),
                          DataColumn(
                            label: ConstrainedBox(
                              constraints: BoxConstraints(
                                maxWidth: 50,
                                minWidth: 50,
                              ),
                              child: Text('Segment'),
                            ),
                          ),
                          DataColumn(
                            onSort: (columnIndex, ascending) => true,
                            label: Text('Account Manager'),
                          ),
                          DataColumn(
                            label: ConstrainedBox(
                              constraints: BoxConstraints(
                                maxWidth: 100,
                                minWidth: 50,
                              ),
                              child: Text('No. Kontrak'),
                            ),
                          ),
                          DataColumn(label: Text('Nama_Pelanggan')),
                          DataColumn(label: Text('Action')),
                          DataColumn(
                            label: ConstrainedBox(
                              constraints: BoxConstraints(
                                maxWidth: 50,
                                minWidth: 50,
                              ),
                              child: Text(''),
                            ),
                          ),
                        ],
                        rows: snapShot.data['data'].map<DataRow>((e) {
                          var data = snapShot.data['data'];
                          var segment = data;
                          return DataRow(cells: [
//                        DataCell(Text((n++).toString())),
                            DataCell(Text(e["segment"]["acronym"].toString())),
                            DataCell(Text(e["user"]["name"].toString())),
                            DataCell(Text(e["contract_number"])),
                            DataCell(Text(e["customer_name"])),
                            DataCell(RaisedButton(
                              color: Colors.redAccent,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              onPressed: () {},
                              child: Text(
                                "Amandemen",
                                style: TextStyle(color: Colors.white),
                              ),
                            )),
                            DataCell(RaisedButton(
                              color: Colors.redAccent,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              onPressed: () {},
                              child: Text("Do",
                                  style: TextStyle(color: Colors.white)),
                            ))
                          ]);
                        }).toList()),
                  ),
                ],
              ),
            );
          }
        });

    return Container(
        padding: EdgeInsets.all(GenDimen.paddingUtama),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Contact Reminder",
                style: TextStyle(fontSize: GenDimen.fontTitle),
              ),
              SizedBox(
                height: GenDimen.paddingSub,
              ),
              SizedBox(
                height: GenDimen.paddingSub,
              ),
//               Row(
//                 crossAxisAlignment: CrossAxisAlignment.end,
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   StreamBuilder<Object>(
//                       stream: bloc.dashboard.listSegment,
//                       builder: (BuildContext context,
//                           AsyncSnapshot<dynamic> snapShot) {
//                         if (!snapShot.hasData) {
//                           return Expanded(child: GenTextShimmer("Segment"));
//                         } else {
// //                          return Expanded(child: GenTextShimmer("Segment"));

//                           return Expanded(
//                               child: GenDropDownMap(
//                             "SEGMENT",
//                             snapShot.data,
//                             segment,
//                             (tap) {
//                               setState(() => segment = tap);
//                             },
//                             id: "id",
//                             val: "name",
//                           ));
//                         }
//                       }),
//                   SizedBox(
//                     width: 10,
//                   ),
//                   StreamBuilder<Object>(
//                       stream: bloc.dashboard.listAm,
//                       builder: (BuildContext context,
//                           AsyncSnapshot<dynamic> snapShot) {
//                         if (!snapShot.hasData) {
//                           return Expanded(child: GenTextShimmer("Segment"));
//                         } else {
// //                          return Expanded(child: GenTextShimmer("Segment"));

//                           return Expanded(
//                               child: GenDropDownMap(
//                             "ACCOUNT MANAGER",
//                             snapShot.data,
//                             am,
//                             (tap) {
//                               setState(() => am = tap);
//                             },
//                             id: "id",
//                             val: "name",
//                           ));
//                         }
//                       }),
//                   SizedBox(
//                     width: 10,
//                   ),
//                   Expanded(
//                     child: GenDropDown("Status", listStatus, status, (val) {
//                       setState(() {
//                         status = val;
//                       });
//                     }),
//                   ),
//                   SizedBox(
//                     width: 10,
//                   ),
//                   Expanded(
//                       child: GenButton(
//                     "Search",
//                     () {},
//                     col: Colors.white,
//                     icon: Icons.search,
//                     bgCol: GenColor.red,
//                     pad: 10,
//                   ))
//                 ],
 //             ),
//              SizedBox(height: 30),
              Expanded(
                child: ListView(
                children: <Widget>[
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Container(
                    //width: size.width,
                    height: 396,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        reminderOfTheDay,
                      ],
                    ),
                  ),
                ]),
              ],
            ),
              )
            ]));
  }
}
