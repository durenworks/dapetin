import 'package:dapetinapp/common/GenColor.dart';
import 'package:flutter/material.dart';

class WideSidebar extends StatelessWidget {
  final double width;
  WideSidebar(this.width);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(10),
      width: width,
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              children: <Widget>[
                Column(children: [
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: 35,
                          width: 35,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/profile-photo-copy.png'))),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 10),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(""),
                                Text(
                                  "",
                                  style: TextStyle(
                                      color: GenColor.grey, fontSize: 12),
                                ),
                              ]),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: 35,
                          width: 35,
                          margin: EdgeInsets.only(right: 10),
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/home-icon.png'))),
                        ),
                        Text("Home"),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: 35,
                          width: 35,
                          margin: EdgeInsets.only(right: 10),
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/icon-dashboard.png'))),
                        ),
                        Text("Input Contract"),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: 35,
                          width: 35,
                          margin: EdgeInsets.only(right: 10),
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/input-icon.png'))),
                        ),
                        Text("Input Data"),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: 35,
                          width: 35,
                          margin: EdgeInsets.only(right: 10),
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/contact-reminder.png'))),
                        ),
                        Text("Contract Reminder"),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: 35,
                          width: 35,
                          margin: EdgeInsets.only(right: 10),
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/technology.png'))),
                        ),
                        Text("Database"),
                      ],
                    ),
                  ),
                  Container(
                    child: Column(
                      children: <Widget>[
                        SizedBox(height:20.0),
                        ExpansionTile(
                          leading: Container(margin: EdgeInsets.symmetric(vertical: 10),
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          height: 35,
                                          width: 35,
                                          margin: EdgeInsets.only(right: 10),
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      'assets/images/technology.png'))),
                                          ),
                                        ],
                                        ),
                                      ),
                          title: Text(
                            "Title",
                            style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold
                            ),
                          ),
                          children: <Widget>[
                            ExpansionTile(
                              title: Text(
                                'Sub title',
                              ),
                              children: <Widget>[
                                ListTile(
                                  title: Text('data'),
                                )
                              ],
                            ),
                            ListTile(
                              title: Text(
                                'data'
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ]),
              ],
            ),
          ),
          Container(
            child: Row(
              children: <Widget>[
                Container(
                  height: 35,
                  width: 35,
                  margin: EdgeInsets.only(right: 10),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/images/toggle.png'))),
                ),
                Text("Toggle sidebar"),
              ],
            ),
          )
        ],
      ),
    );
  }
}
