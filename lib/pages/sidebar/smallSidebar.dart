import 'package:dapetinapp/common/GenColor.dart';
import 'package:flutter/material.dart';

class SmallSidebar extends StatelessWidget {
  const SmallSidebar({Key key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(10),
      width: 70,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: ListView(
              children: <Widget>[
                Column(children: [
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: Container(
                      height: 35,
                      width: 35,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: AssetImage(
                                  'assets/images/profile-photo-copy.png'))),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: Container(
                        height: 35,
                        width: 35,
                        margin: EdgeInsets.only(right: 10),
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image:
                                    AssetImage('assets/images/home-icon.png'))),
                      )),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: Container(
                      height: 35,
                      width: 35,
                      margin: EdgeInsets.only(right: 10),
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                  'assets/images/icon-dashboard.png'))),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: Container(
                      height: 35,
                      width: 35,
                      margin: EdgeInsets.only(right: 10),
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image:
                                  AssetImage('assets/images/input-icon.png'))),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: Container(
                      height: 35,
                      width: 35,
                      margin: EdgeInsets.only(right: 10),
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                  'assets/images/contact-reminder.png'))),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: Container(
                      height: 35,
                      width: 35,
                      margin: EdgeInsets.only(right: 10),
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image:
                                  AssetImage('assets/images/technology.png'))),
                    ),
                  )
                ]),
              ],
            ),
          ),
          Container(
            child: Container(
              height: 35,
              width: 35,
              margin: EdgeInsets.only(right: 10),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/toggle.png'))),
            ),
          )
        ],
      ),
    );
  }
}
