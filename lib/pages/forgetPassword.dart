import 'package:dapetinapp/common/GenColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ForgetPassword extends StatefulWidget {
  const ForgetPassword({Key key}) : super(key: key);

  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context);
    var size = media.size;
    var orientation = media.orientation;
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: Colors.white,
          width: size.width,
          height: size.height,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              orientation == Orientation.landscape
                  ? Container(
                      width: size.width / 2,
                      height: size.height,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image:
                                  AssetImage('assets/images/image-login.png'))),
                    )
                  : Container(),
              Expanded(
                child: ListView(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 70),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image.asset('assets/images/logo-dapetin.png'),
                          Container(
                            margin: EdgeInsets.only(top: 50, bottom: 9),
                            child: Text(
                              "Forget Password",
                              style: TextStyle(
                                  fontSize: 32, fontWeight: FontWeight.w500),
                            ),
                          ),
                          Container(
                            width: size.width / 1.5,
                            child: Text(
                              "If you forget your password, please contact your administrator",
                              textAlign: TextAlign.center,
                              style: TextStyle(color: GenColor.grey),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 30),
                            child: Column(children: [
                              Form(
                                  key: _formKey,
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Row(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Column(
                                              children: <Widget>[
                                                Container(
                                                  height: 32,
                                                  margin: EdgeInsets.only(
                                                      bottom: 10, right: 10),
                                                  child: Image.asset(
                                                      'assets/images/local-phone-material.png'),
                                                ),
                                                Container(
                                                  height: 32,
                                                  margin: EdgeInsets.only(
                                                      bottom: 10, right: 10),
                                                  child: Image.asset(
                                                      'assets/images/email-material.png'),
                                                ),
                                                Container(
                                                  height: 32,
                                                  margin: EdgeInsets.only(
                                                      bottom: 10, right: 10),
                                                  child: Image.asset(
                                                      'assets/images/telegram-font-awesome.png'),
                                                ),
                                                Container(
                                                  height: 32,
                                                  margin: EdgeInsets.only(
                                                      bottom: 10, right: 10),
                                                  child: Image.asset(
                                                      'assets/images/whatsapp-font-awesome.png'),
                                                ),
                                              ],
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Container(
                                                  height: 32,
                                                  margin: EdgeInsets.only(
                                                      bottom: 10, right: 10),
                                                  child: Center(
                                                    child: Text(
                                                      "021-7564 1234",
                                                      style: TextStyle(
                                                          color: GenColor.grey),
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  height: 32,
                                                  margin: EdgeInsets.only(
                                                      bottom: 10, right: 10),
                                                  child: Center(
                                                    child: Text(
                                                      "admin@dapetin.com",
                                                      style: TextStyle(
                                                          color: GenColor.grey),
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  height: 32,
                                                  margin: EdgeInsets.only(
                                                      bottom: 10, right: 10),
                                                  child: Center(
                                                    child: Text(
                                                      "@admindapetin",
                                                      style: TextStyle(
                                                          color: GenColor.grey),
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  height: 32,
                                                  margin: EdgeInsets.only(
                                                      bottom: 10, right: 10),
                                                  child: Center(
                                                    child: Text(
                                                      "08123 123 123",
                                                      style: TextStyle(
                                                          color: GenColor.grey),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        Container(
                                          height: 20,
                                        ),
                                        Center(
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Text(
                                                "Have an account?",
                                                style: TextStyle(
                                                  color: GenColor.grey,
                                                ),
                                              ),
                                              Text(
                                                "Login hire",
                                                textAlign: TextAlign.end,
                                                style: TextStyle(
                                                    color: GenColor.blue),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          height: 10,
                                        ),
                                        Center(
                                          child: Text(
                                            "Don't have an account?",
                                            style: TextStyle(
                                              color: GenColor.grey,
                                            ),
                                          ),
                                        ),
                                        Center(
                                          child: Text(
                                            "Contact administrator",
                                            textAlign: TextAlign.end,
                                            style:
                                                TextStyle(color: GenColor.blue),
                                          ),
                                        )
                                      ]))
                            ]),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
