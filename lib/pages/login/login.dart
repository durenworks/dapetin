import 'package:dapetinapp/api/request.dart';
import 'package:dapetinapp/common/GenColor.dart';
import 'package:dapetinapp/component/element/genPreferrence.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';

class Login extends StatefulWidget {
  const Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  String _username, _password;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _isLoading = false;
  }



  Future<void> _submit(context) async {
    final form = _formKey.currentState;

    if (form.validate()) {
      form.save();
      setState(() => _isLoading = true);

       if( await postLogin(_username, _password)){
        print("sukses");
        Toast.show("Login Berhasil!", context, duration: Toast.LENGTH_LONG);
        setState(() => _isLoading = false);
        Navigator.of(context).pushReplacementNamed("/dashboard");
      }else{
        //print(_username + " pass " + _password);
        Toast.show("Maaf, Email / Password anda salah!", context, duration: Toast.LENGTH_LONG);
        setState(() => _isLoading = false);
      }

//        get(_username);
//        print("try login");
    }
  }


  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context);
    var size = media.size;
    var orientation = media.orientation;

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);

    return Scaffold(
      body: SafeArea(
        child: Container(
          color: Colors.white,
          width: size.width,
          height: size.height,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              orientation == Orientation.landscape
                  ? Container(
                      width: size.width / 2,
                      height: size.height,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image:
                                  AssetImage('assets/images/image-login.png'))),
                    )
                  : Container(),
              Expanded(
                child: ListView(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 70),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image.asset('assets/images/logo-dapetin.png'),
                          Container(
                              margin: EdgeInsets.only(top: 50, bottom: 9),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("Sign in to ",
                                      style: TextStyle(
                                          fontSize: 32,
                                          fontWeight: FontWeight.w100)),
                                  Text(
                                    "Dapetin",
                                    style: TextStyle(
                                        fontSize: 32,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ],
                              )),
                          Text(
                            "Please enter your account to proceed",
                            style: TextStyle(color: GenColor.grey),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(
                                vertical: 48, horizontal: 50),
                            child: Column(children: [
                              Form(
                                  key: _formKey,
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "EMAIL ADDRESS",
                                          style: TextStyle(
                                              color: GenColor.grey,
                                              fontWeight: FontWeight.w500,
                                              letterSpacing: 2),
                                        ),
                                        Container(
                                          height: 8,
                                        ),
                                        TextFormField(
//                                          controller: _controllerEmail,
                                          onSaved: (val) {
                                            setState(() {
                                              _username = val;
                                            });
                                          },
                                          validator: (val) {
                                            return val.length < 5
                                                ? "Email Address must have atleast 10 chars"
                                                : null;
                                          },
                                          decoration: InputDecoration(
                                              fillColor: GenColor.lightGrey,
                                              border: new OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: GenColor.grey,
                                                    width: 1),
                                                borderRadius:
                                                    new BorderRadius.circular(
                                                        5.0),
                                              ),
                                              focusedBorder:
                                                  new OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: GenColor.grey,
                                                    width: 1),
                                                borderRadius:
                                                    new BorderRadius.circular(
                                                        5.0),
                                              ),
                                              contentPadding: EdgeInsets.all(5),
                                              filled: true),
                                        ),
                                        Container(
                                          height: 26,
                                        ),
                                        Text(
                                          "PASSWORD",
                                          style: TextStyle(
                                              color: GenColor.grey,
                                              fontWeight: FontWeight.w500,
                                              letterSpacing: 2),
                                        ),
                                        Container(
                                          height: 8,
                                        ),
                                        TextFormField(
//                                          controller: _controllerPassword,
                                          obscureText: true,
                                          onSaved: (val) => _password = val,
                                          decoration: InputDecoration(
                                            fillColor: GenColor.lightGrey,
                                            border: new OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: GenColor.grey,
                                                  width: 1),
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      5.0),
                                            ),
                                            focusedBorder:
                                                new OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: GenColor.grey,
                                                  width: 1),
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      5.0),
                                            ),
                                            contentPadding: EdgeInsets.all(5),
                                            suffixIcon: Padding(
                                                padding: EdgeInsets.all(0.0),
                                                child: Image.asset(
                                                    'assets/images/eye-slash.png')),
                                            filled: true,
                                          ),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Checkbox(
                                                checkColor: Colors.white,
                                                activeColor: GenColor.orange,
                                                value: true,
                                                onChanged: (cvalue) {}),
                                            Text(
                                              "Remember Me",
                                              style: TextStyle(
                                                  color: GenColor.grey),
                                            ),
                                            Expanded(
                                                child: new FlatButton(
                                              onPressed: () {
                                                Navigator.of(context)
                                                    .pushReplacementNamed(
                                                        "/forget_password");
                                              },
                                              child: Text("Forgot Password?",
                                                  style: TextStyle(
                                                      color: Colors.blue,
                                                      decoration: TextDecoration
                                                          .underline)),
                                            ))
                                          ],
                                        ),
                                        Container(
                                          height: 10,
                                        ),
                                        _isLoading
                                            ? new CircularProgressIndicator()
                                            : RaisedButton(
                                                onPressed: () {
                                                  _submit(context);
                                                },
                                                color: GenColor.red,
                                                textColor: Colors.white,
                                                padding: EdgeInsets.all(15),
                                                elevation: 0,
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Text("Sign In"),
                                                  ],
                                                ),
                                              ),
                                        Container(
                                          height: 10,
                                        ),
                                        Center(
                                          child: Text(
                                            "Don't have an account?",
                                            style: TextStyle(
                                              color: GenColor.grey,
                                            ),
                                          ),
                                        ),
                                        Container(
                                          height: 10,
                                        ),
                                        Center(
                                          child: Text(
                                            "Contact administrator",
                                            textAlign: TextAlign.end,
                                            style:
                                                TextStyle(color: GenColor.blue),
                                          ),
                                        )
                                      ]))
                            ]),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
     );
  }
}
