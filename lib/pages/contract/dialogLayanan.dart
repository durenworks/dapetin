import 'package:dapetinapp/blocs/baseBloc.dart';
import 'package:dapetinapp/blocs/note_bloc.dart';
import 'package:dapetinapp/common/GenColor.dart';
import 'package:dapetinapp/component/element/GenButton.dart';
import 'package:dapetinapp/component/element/genDropDownMap.dart';
import 'package:dapetinapp/component/element/genTextFormField.dart';
import 'package:dapetinapp/component/element/genTextShimmer.dart';
import 'package:dapetinapp/models/layanan.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';

class DialogLayanan extends StatefulWidget {
  @override
  _DialogLayananState createState() => _DialogLayananState();
}

class _DialogLayananState extends State<DialogLayanan> {
  final _formKey = GlobalKey<FormState>();

  static String patttern = r'(^[0-9]*$)';
  RegExp validateNumber = new RegExp(patttern);

  var bloc;
  String noKontrak = "tes no kontrak";
  String sid;
  String jenisLayanan;
  String layanan;
  String paket;
  String paketBandwith;
  String alamatInstalasi;
  String biayaInstalasi;
  String biayaBulanan;
  String keterangan;

  void simpanLayanan(context) {
    final form = _formKey.currentState;
    form.save();

    if (form.validate()) {


      bloc.inSink.add(Layanan(
          noKontrak: noKontrak,
          sid: sid,
          jenisLayanan: jenisLayanan,
          layanan: layanan,
          paket: paket,
          paketBandwith: paketBandwith,
          alamatInstalasi: alamatInstalasi,
          biayaInstalasi: biayaInstalasi,
          biayaBulanan: biayaBulanan,
          keterangan: keterangan,
          state: NotesState.INSERT));

      Toast.show("Layanan berhasil di tambahkan.", context,
          duration: Toast.LENGTH_LONG);


      Navigator.of(context).pop();
      //print("sukses input");
    } else {
      print("error");
      Toast.show("Periksa kembali inputanmu.", context,
          duration: Toast.LENGTH_LONG);
    }
  }

  List listLayananKosong = [
    {"id": "1", "name": "Silahkan pilih type layanan"},
  ];

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<BaseBloc>(context);
    bloc.inSink.add(Layanan(state: NotesState.GETALL));

//    blok = Provider.of<NoteBloc>(context);
    bloc.dashboard.getListListSegment();
    bloc.dashboard.getListAm();
    bloc.dashboard.getListServiceType();

    jenisLayanan != null
        ? bloc.dashboard.getListService(jenisLayanan)
        : bloc.dashboard.getListService("1");

    return Container(
        padding: EdgeInsets.all(30),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Text(
                  'TAMBAHKAN LAYANAN',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Icon(
                    Icons.cancel,
                    color: Colors.redAccent,
                  ),
                )
              ]),
              SizedBox(
                height: 30,
              ),
              Divider(),
              SizedBox(
                height: 20,
              ),
              GenTextFromField(
                "SID",
                validator: (value) {
                  if (value.isEmpty) {
                    return "SID tidak boleh kososng!";
                  } else {
                    return null;
                  }
                },
                onSaved: (val) {
                  sid = val;
                },
              ),

              SizedBox(
                height: 20,
              ),
              StreamBuilder<Object>(
                  stream: bloc.dashboard.listServiceType,
//              stream: bloc.dashboard.listSegment,
                  builder:
                      (BuildContext context, AsyncSnapshot<dynamic> snapShot) {
                    if (!snapShot.hasData) {
                      return GenTextShimmer("JENIS LAYANAN");
                    } else {
                      //print("do");
//                    setState(() {
//
//                    });
                      return GenDropDownMap(
                        "JENIS LAYANAN",
                        snapShot.data,
                        jenisLayanan,
                        (val) {
                          setState(() {
                            jenisLayanan = val;
                            layanan = null;
                          });
                        },
                        id: "id",
                        val: "name",
                      );
                    }
                  }),

              SizedBox(
                height: 20,
              ),

              StreamBuilder<Object>(
                  stream: bloc.dashboard.listService,
                  builder:
                      (BuildContext context, AsyncSnapshot<dynamic> snapShot) {
                    if (!snapShot.hasData) {
                      return GenDropDownMap(
                        "LAYANAN",
                        listLayananKosong,
                        null,
                        (val) {},
                        id: "id",
                        val: "name",
                      );
                    }
                    return GenDropDownMap(
                      "LAYANAN",
                      jenisLayanan != null ? snapShot.data : listLayananKosong,
                      layanan,
                      (val) {
                        setState(() {
                          layanan = val;
                        });
                      },
                      id: "id",
                      val: "name",
                    );
                  }),
//        bloc.dashboard.getListService();
              SizedBox(
                height: 20,
              ),
              GenTextFromField(
                "PAKET",
                validator: (value) {
                  if (value.isEmpty) {
                    return "Paket tidak boleh kososng!";
                  } else {
                    return null;
                  }
                },
                onSaved: (val) {
                  paket = val;
                },
              ),

              SizedBox(
                height: 20,
              ),
              GenTextFromField(
                "PAKET BANDWIDTH",
                validator: (value) {
                  if (value.isEmpty) {
                    return "Paket Bandwidth tidak boleh kososng!";
                  } else {
                    return null;
                  }
                },
                onSaved: (val) {
                  paketBandwith = val;
                },
              ),

              SizedBox(
                height: 20,
              ),
              GenTextFromField(
                "ALAMAT INSTALASI",
                onSaved: (val) {
                  alamatInstalasi = val;
                },
              ),

              SizedBox(
                height: 20,
              ),
              GenTextFromField(
                "BIAYA INSTALASI",
                validator: (value) {
                  if (value.isEmpty) {
                    return "Biaya insatalasi tidak boleh kosong!";
                  } else if (!validateNumber.hasMatch(value)) {
                    return "Biaya insatalasi harus di isi dengan angka!";
                  } else {
                    return null;
                  }
                },
                onSaved: (val) {
                  biayaInstalasi = val;
                },
              ),

              SizedBox(
                height: 20,
              ),
              GenTextFromField(
                "BIAYA BULANAN",
                validator: (value) {
                  if (value.isEmpty) {
                    return "Biaya bulanan tidak boleh kosong!";
                  } else if (!validateNumber.hasMatch(value)) {
                    return "Biaya bulanan harus di isi dengan angka!";
                  } else {
                    return null;
                  }
                },
                onSaved: (val) {
                  biayaBulanan = val;
                },
              ),

              SizedBox(
                height: 20,
              ),
              GenTextFromField(
                "KETERANGAN",
                validator: (value) {
                  if (value.isEmpty) {
                    return "Keterangan tidak boleh kososng!";
                  } else {
                    return null;
                  }
                },
                onSaved: (val) {
                  keterangan = val;
                },
              ),

              SizedBox(
                height: 20,
              ),
//          StreamBuilder(
//            stream: bloc.outgoing,
//            builder: (context, AsyncSnapshot<List<Layanan>> snapshot) {
//              if (!snapshot.hasData) {
//                return CircularProgressIndicator();
//              }
//              return Column(
//                children: snapshot.data
//                    .map(
//                      (note) => GestureDetector(
//                        child: ListTile(
//                          title: note.editing
//                              ? TextFormField(
//                                  initialValue: note.noKontrak,
//                                  onFieldSubmitted: (text) => setState(
//                                    () => bloc.inSink.add(
//                                      note.copyWith(
//                                        noKontrak: text,
//                                        editing: !note.editing,
//                                        state: NotesState.UPDATE,
//                                      ),
//                                    ),
//                                  ),
//                                )
//                              : Text(note.sid),
//                          trailing: IconButton(
//                            icon: Icon(Icons.delete),
//                            onPressed: () => setState(
//                              () => bloc.inSink.add(
//                                note.copyWith(
//                                  id: note.id,
//                                  state: NotesState.DETLETE,
//                                ),
//                              ),
//                            ),
//                          ),
//                        ),
//                        onTap: () {
//                          setState(() => bloc.inSink.add(
//                                note.copyWith(
//                                    editing: !note.editing,
//                                    state: NotesState.UPDATE),
//                              ));
//                        },
//                      ),
//                    )
//                    .toList(),
//              );
//            },
//          ),
              Align(
                alignment: Alignment.centerRight,
                child: GenButton("SIMPAN", () => simpanLayanan(context),
                    bgCol: GenColor.red, col: Colors.white, pad: 20),
              )
            ],
          ),
        ));
  }
}
