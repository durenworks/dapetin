import 'dart:async';
import 'dart:convert';

import 'package:dapetinapp/api/request.dart';
import 'package:dapetinapp/blocs/baseBloc.dart';
import 'package:dapetinapp/common/GenColor.dart';
import 'package:dapetinapp/common/GenDimen.dart';
import 'package:dapetinapp/component/element/GenButton.dart';
import 'package:dapetinapp/component/element/genButtonDate.dart';
import 'package:dapetinapp/component/element/genCardLayanan.dart';
import 'package:dapetinapp/component/element/genDropDown.dart';
import 'package:dapetinapp/component/element/genDropDownMap.dart';
import 'package:dapetinapp/component/element/genPreferrence.dart';
import 'package:dapetinapp/component/element/genTextFormField.dart';
import 'package:dapetinapp/component/element/genTextShimmer.dart';
import 'package:dapetinapp/models/Kontrak.dart';
import 'package:dapetinapp/models/layanan.dart';
import 'package:dapetinapp/pages/contract/dialogLayanan.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'package:toast/toast.dart';

class ContractPage extends StatefulWidget {
  @override
  _ContractPageState createState() => _ContractPageState();
}

class _ContractPageState extends State<ContractPage> {
//  final _formKontrak = GlobalKey<FormState>();
  final _formDataPelanggan = GlobalKey<FormState>();
  bool _isLoading = false;

//  Kontrak kontrak = new Kontrak();
  var data;
  int stepIndex = 0;
  bool complete = false;
  bool active = false;
  var bloc;

  //DATA PELANGGAN
  var kontrakId;
  var noKontrak;
  var segment;
  var am;
  var namaPelanggan;
  var npwp;
  var alamatPelanggan;

  //ttd
  var namaPic1;
  var jabatan1;
  var namaPic2;
  var jabatan2;
  var emailpic1;
  var emailpic2;

  //JANGKA WAKTU
  var startDate;
  var endDate;
  var masaBerlaku;

  //LIST LAYANAN
  List listLayanan = [];

  //TEXT CONTROLLER
  var cnoKontrak = TextEditingController();
  var cnpwp = TextEditingController();
  var cnamaPelanggan = TextEditingController();
  var calamatPelanggan = TextEditingController();
  var cnamaPic1 = TextEditingController();
  var cnamaPic2 = TextEditingController();
  var cjabatan1 = TextEditingController();
  var cjabatan2 = TextEditingController();
  var cemailpic1 = TextEditingController();
  var cemailpic2 = TextEditingController();

  Future<void> fillForm() async {
    await getPrefferenceEditKontrak().then((value) async {
      if (value != null) {
        await postGetContractDetail("token", value).then((value2) {
          var snapdata = value2['data'][0];
          print(snapdata);
          kontrakId = snapdata['id'];
          noKontrak = snapdata['contract_number'];
          namaPelanggan = snapdata['customer_name'];
          npwp = snapdata['customer_npwp'];
          alamatPelanggan = snapdata['customer_address'];
          segment = snapdata['segment_id'].toString();
          namaPic1 = snapdata['customer_pic1_name'].toString();
          jabatan1 = snapdata['customer_pic1_position'].toString();
          emailpic1 = snapdata['customer_pic1_email'].toString();
          namaPic2 = snapdata['customer_pic2_name'].toString();
          jabatan2 = snapdata['customer_pic2_position'].toString();
          emailpic2 = snapdata['customer_pic2_email'].toString();
          DateTime start = DateTime.parse(snapdata['start_date']);
          DateTime end = DateTime.parse(snapdata['end_date']);
          startDate = DateFormat("yyyy-MM-dd").format(start);
          endDate = DateFormat("yyyy-MM-dd").format(end);
          masaBerlaku = snapdata['duration'].toString();

          cnoKontrak.text = noKontrak;
          cnamaPelanggan.text = namaPelanggan;
          cnpwp.text = npwp;
          calamatPelanggan.text = alamatPelanggan;
          cnamaPic1.text = namaPic1;
          cjabatan1.text = jabatan1;
          cemailpic1.text = emailpic1;
          cnamaPic2.text = namaPic2;
          cjabatan2.text = jabatan2;
          cemailpic2.text = emailpic2;

          //PARSING LAYANAN
          bloc.inSink.add(Layanan(state: NotesState.DELETE_ALL));
          var services = snapdata['services'];
          print(services);
          for (var i = 0; i < snapdata['services'].length; i++) {
            var id = services[i]['id'];
            var sid = services[i]['sid'].toString();
            var noKontrak = services[i]['contract_id'].toString();
            var jenisLayanan = services[i]['service_type_id'].toString();
            var layanan = services[i]['service_id'].toString();
            var paket = services[i]['package'].toString();
            var paketBandwith = services[i]['bandwith_package'].toString();
            var biayaInstalasi = services[i]['instalation_fee'];
            var biayaBulanan = services[i]['monthly_fee'];
            var keterangan = services[i]['note'].toString();

            bloc.inSink.add(Layanan(id: id, sid: sid, noKontrak: noKontrak, jenisLayanan: jenisLayanan, layanan: layanan, paket: paket, paketBandwith: paketBandwith,
                biayaInstalasi: biayaInstalasi, biayaBulanan: biayaBulanan, keterangan: keterangan, state: NotesState.INSERT));
          }

          delPrefferenceEditKontrak();
          setState(() {});
          print(noKontrak);
        });
      }
    });
//      print(snapdata['contract_number']);

//      //DATA PELANGGAN
//      noKontrak = snapdata['contract_number'];
//      segment = data;
//      am;
//      namaPelanggan;
//      npwp;
//      alamatPelanggan;
//
//      //ttd
//      namaPic1;
//      jabatan1;
//      namaPic2;
//      jabatan2;
//      emailpic1;
//      emailpic2;
//
//      //JANGKA WAKTU
//      startDate;
//      endDate;
//      masaBerlaku;
  }

  @override
  void initState() {
    super.initState();
    _isLoading = false;
    fillForm();
  }

  void simpanKontrak(context) async {
    setState(() => _isLoading = true);

    if (listLayanan.length > 0) {
      if (await postAddKontrak(
          noKontrak,
          segment,
          am,
          namaPelanggan,
          npwp,
          alamatPelanggan,
          namaPic1,
          jabatan1,
          namaPic1,
          jabatan2,
          emailpic1,
          emailpic2,
          masaBerlaku,
          startDate,
          endDate,
          listLayanan)) {
        bloc.inSink.add(Layanan(state: NotesState.DELETE_ALL));
        Toast.show("Data kontrak berhasil di masukan.", context,
            duration: Toast.LENGTH_LONG);
        setState(() => _isLoading = false);
      } else {
        Toast.show("Gagal memasukan data kontrak, coba cek kembali.", context,
            duration: Toast.LENGTH_LONG);
        setState(() => _isLoading = false);
      }
    } else {
      Toast.show("Kamu harus menambahkan layanan.", context,
          duration: Toast.LENGTH_LONG);
      setState(() => _isLoading = false);
    }
  }

  //LAYANAN

  List listLayananKosong = [
    {"id": "1", "name": "Silahkan pilih type layanan"},
  ];

  List listMasaBerlaku = [
    {"id": 1, "val": "1 Bulan"},
    {"id": 2, "val": "2 Bulan"},
    {"id": 3, "val": "3 Bulan"},
    {"id": 4, "val": "4 Bulan"},
    {"id": 5, "val": "5 Bulan"},
    {"id": 6, "val": "6 Bulan"},
    {"id": 7, "val": "7 Bulan"},
    {"id": 8, "val": "8 Bulan"},
    {"id": 9, "val": "9 Bulan"},
    {"id": 10, "val": "10 Bulan"},
    {"id": 11, "val": "11 Bulan"},
    {"id": 12, "val": "12 Bulan"},
  ];
  DateTime selectedDate = DateTime.now();
  DateTime selectedDate2 = DateTime.now();

  void deleteLayanan(Layanan e) {
    bloc.inSink.add(e.copyWith(id: e.id, state: NotesState.DETLETE));
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2020, 1),
        lastDate: DateTime(2101));

    if (picked != null) {
      setState(() {
        //print(selectedDate);
        startDate = DateFormat("yyyy-MM-dd").format(selectedDate);
        //print(masaBerlaku);
        //DateTime _endDate;
        if (masaBerlaku != null) {
          var _number = int.parse(masaBerlaku);
          var _endDate = new DateTime(selectedDate.year,
              selectedDate.month + _number, selectedDate.day);
          endDate = DateFormat("yyyy-MM-dd").format(_endDate);
          print(_endDate);
        }

        //print(todayDate);
      });
    }
  }

  Future<Null> _selectDate2(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate2,
        firstDate: DateTime(2020, 1),
        lastDate: DateTime(2101));
    if (picked != null) {
      setState(() {
        endDate = DateFormat("yyyy-MM-dd").format(selectedDate2);
      });
    }
  }

  goTo(int step) {
    setState(() {
      stepIndex = step;
    });
  }

  next() {
    if (stepIndex == 1) {
      if (_formDataPelanggan.currentState.validate()) {
        print("if validate");
        stepIndex + 1 != 3
            ? goTo(stepIndex + 1)
            : setState(() => complete = true);
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text('Processing Data')));
      } else {
        Toast.show("Inputan mu belum benar, coba cek kembali.", context,
            duration: Toast.LENGTH_LONG);
      }
      print("ini halaman lewat 1");
    } else if (stepIndex == 2) {
      print("ini halaman lewat 2");
    }
  }

  cancel() {
    if (stepIndex > 0) {
      goTo(stepIndex - 1);
    }
  }

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context);
    var size = media.size;
    var orientation = media.orientation;

    bloc = Provider.of<BaseBloc>(context);
    bloc.dashboard.getListListSegment();
    bloc.dashboard.getListAm();
    bloc.dashboard.getListServiceType();

    bloc.inSink.add(Layanan(state: NotesState.GETALL));

//    print(noKontrak);
//    print(namaLayanan);
    var dataPelanggan = Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10), color: Colors.white),
      padding: EdgeInsets.all(orientation == Orientation.portrait ? 10 : 30),
      child: Form(
        key: _formDataPelanggan,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "DATA PELANGGAN",
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(
              height: 15,
            ),
            Divider(),
            SizedBox(
              height: 15,
            ),
            GenTextFromField("NO. KONTRAK", controller: cnoKontrak,
                onSaved: (val) {
              noKontrak = val;
            }, validator: (val) {
              return val.length < 1 ? "No Kontrak Tidak Boleh Kosong" : null;
            }),
            SizedBox(
              height: 15,
            ),
            StreamBuilder<Object>(
                stream: bloc.dashboard.listSegment,
                builder:
                    (BuildContext context, AsyncSnapshot<dynamic> snapShot) {
                  if (!snapShot.hasData) {
                    return GenTextShimmer("SEGMENT");
                  } else {
                    return GenDropDownMap(
                      "SEGMENT",
                      snapShot.data,
                      segment,
                      (tap) {
                        setState(() => segment = tap);
                      },
                      id: "id",
                      val: "name",
                    );
                  }
                }),
            SizedBox(
              height: 15,
            ),
            StreamBuilder<Object>(
                stream: bloc.dashboard.listAm,
                builder:
                    (BuildContext context, AsyncSnapshot<dynamic> snapShot) {
                  if (!snapShot.hasData) {
                    return GenTextShimmer("ACCOUNT MANAGER");
                  } else {
                    return GenDropDownMap("ACCOUNT MANAGER", snapShot.data, am,
                        (tap) {
                      setState(() => am = tap);
                    }, id: "id", val: "name");
                  }
                }),
            SizedBox(
              height: 15,
            ),
            GenTextFromField("NAMA PELANGGAN", controller: cnamaPelanggan,
                onSaved: (val) {
              namaPelanggan = val;
            }, validator: (val) {
              return val.length < 1
                  ? "Nama Pelanggan Tidak Boleh Kosong"
                  : null;
            }),
            SizedBox(
              height: 15,
            ),
            GenTextFromField("NPWP", controller: cnpwp, onSaved: (val) {
              npwp = val;
            }, validator: (val) {
              return val.length < 1 ? "NPWP Tidak Boleh Kosong" : null;
            }),
            SizedBox(
              height: 15,
            ),
            GenTextFromField("ALAMAT PELANGGAN", controller: calamatPelanggan,
                onSaved: (val) {
              alamatPelanggan = val;
            }, validator: (val) {
              return val.length < 1
                  ? "Alamat Pelanggan Tidak Boleh Kosong"
                  : null;
            }),
            SizedBox(
              height: 30,
            ),
            Text(
              "PIC BERTANDA TANGAN",
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(
              height: 15,
            ),
            GenTextFromField("NAMA PIC 1", controller: cnamaPic1,
                onSaved: (val) {
              namaPic1 = val;
            }, validator: (val) {
              return val.length < 1 ? "PIC Tidak Boleh Kosong" : null;
            }),
            SizedBox(
              height: 15,
            ),
            GenTextFromField("JABATAN", controller: cjabatan1, onSaved: (val) {
              jabatan1 = val;
            }, validator: (val) {
              return val.length < 1 ? "Jabatan Tidak Boleh Kosong" : null;
            }),
            SizedBox(
              height: 15,
            ),
            GenTextFromField("NAMA PIC 2", controller: cnamaPic2,
                onSaved: (val) {
              namaPic2 = val;
            }, validator: (val) {
              return val.length < 1 ? "Nama Pic 2Tidak Boleh Kosong" : null;
            }),
            SizedBox(
              height: 15,
            ),
            GenTextFromField("JABATAN 2", controller: cjabatan2,
                onSaved: (val) {
              jabatan2 = val;
            }, validator: (val) {
              return val.length < 1 ? "Jabatan Tidak Boleh Kosong" : null;
            }),
            SizedBox(
              height: 15,
            ),
            GenTextFromField("EMAIL PIC 1", controller: cemailpic1,
                onSaved: (val) {
              emailpic1 = val;
            }, validator: (val) {
              return val.length < 1 ? "Email Pic 1 Tidak Boleh Kosong" : null;
            }),
            SizedBox(
              height: 15,
            ),
            GenTextFromField("EMAIL PIC 2", controller: cemailpic2,
                onSaved: (val) {
              emailpic2 = val;
            }, validator: (val) {
              return val.length < 1 ? "Email Pic 2 Tidak Boleh Kosong" : null;
            }),
            SizedBox(
              height: 15,
            ),
            Align(
              alignment: Alignment.centerRight,
              child: _isLoading
                  ? new CircularProgressIndicator()
                  : GenButton("CONTINUE", () {
                      // Validate returns true if the form is valid, or false
                      // otherwise.
                      if (_formDataPelanggan.currentState.validate() &&
                          segment != null &&
                          am != null) {
                        _formDataPelanggan.currentState.save();
                        // If the form is valid, display a Snackbar.
                        stepIndex + 1 != 3
                            ? goTo(stepIndex + 1)
                            : setState(() => complete = true);
                      } else {
//                        stepIndex + 1 != 3
//                            ? goTo(stepIndex + 1)
//                            : setState(() => complete = true);
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: Text('Periksa kembali inputanmu'),
                          backgroundColor: Colors.redAccent,
                        ));
                      }
                    }, bgCol: GenColor.red, col: Colors.white, pad: 15),
            )
          ],
        ),
      ),
    );

    var jangkaWaktu = Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10), color: Colors.white),
      padding: EdgeInsets.all(30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "JANGKA WAKTU",
            style: TextStyle(fontSize: 18),
          ),
          SizedBox(
            height: 15,
          ),
//            GenDropDown("MASA BERLAKU"),
          SizedBox(
            height: 15,
          ),
          GenDropDownMap("MASA BERLAKU", listMasaBerlaku, masaBerlaku, (val) {
            setState(() {
              masaBerlaku = val;
            });
          }, id: "id", val: "val"),
          SizedBox(
            height: 15,
          ),
          GenButtonDate("START DATE", () {
            _selectDate(context);
          }, startDate),
          SizedBox(
            height: 15,
          ),
          GenButtonDate("END DATE", () {
            _selectDate2(context);
          }, endDate),
          SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              GenButton("BACK", () {
                // Validate returns true if the form is valid, or false
                // otherwise.

                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text('Periksa kembali inputanmu'),
                  backgroundColor: Colors.redAccent,
                ));
              }, col: GenColor.red, bgCol: Colors.white, pad: 15),
              SizedBox(
                width: 10,
              ),
              GenButton("CONTINUE", () {
                if (masaBerlaku != null &&
                    startDate != null &&
                    endDate != null) {
                  stepIndex + 1 != 3
                      ? goTo(stepIndex + 1)
                      : setState(() => complete = true);
                } else {
                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text('Periksa kembali inputanmu'),
                    backgroundColor: Colors.redAccent,
                  ));
                }
              }, bgCol: GenColor.red, col: Colors.white, pad: 15)
            ],
          )
        ],
      ),
    );
    var dataLayanan = Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10), color: Colors.white),
      padding: EdgeInsets.all(30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                "LAYANAN",
                style: TextStyle(fontSize: 18),
              ),
              GenButton(
                "Tambahkan Layanan",
                () => showSimpleCustomDialog(context),
                bgCol: GenColor.blue,
                pad: 10,
                col: Colors.white,
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          StreamBuilder(
              stream: bloc.outgoing,
              builder: (context, AsyncSnapshot<List<Layanan>> snapshot) {
                if (!snapshot.hasData) {
                  return Container(
                    height: 200,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Center(child: Text("Silahkan menambahkan layanan.")),
                  );
                } else {
                  listLayanan.clear();
                  print(listLayanan);
                  return Column(
                      children: snapshot.data.map<Widget>((e) {
                    Map<String, dynamic> singleLayanan = {
                      "sid": e.sid,
                      "service_type_id": e.jenisLayanan,
                      "service_id": e.layanan,
                      "package": e.paket,
                      "bandwith_package": e.paketBandwith,
                      "instalation_fee": e.biayaInstalasi,
                      "monthly_fee": e.biayaBulanan,
                      "note": e.keterangan
                    };

                    listLayanan.add(singleLayanan);
                    print(listLayanan);

                    return GenCardLayanan(
                      e.sid,
                      e.jenisLayanan,
                      e.layanan,
                      e.paket,
                      e.paketBandwith,
                      e.biayaInstalasi,
                      e.biayaBulanan,
                      e.alamatInstalasi,
                      e.keterangan,
                      delete: () => deleteLayanan(e),
                    );
                  }).toList());
                }
              }),
          SizedBox(
            height: GenDimen.jarakTextField,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              GenButton("BACK", () {
                // Validate returns true if the form is valid, or false
                // otherwise.
                if (masaBerlaku != null &&
                    startDate != null &&
                    endDate != null) {
//              _formDataPelanggan.currentState.save();
                  // If the form is valid, display a Snackbar.
                  if (stepIndex > 0) {
                    goTo(stepIndex - 1);
                  }
                } else {
                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text('Periksa kembali inputanmu'),
                    backgroundColor: Colors.redAccent,
                  ));
                }
              }, col: GenColor.red, bgCol: Colors.white, pad: 15),
              SizedBox(
                width: 10,
              ),
              GenButton("SIMPAN KONTRAK", () => simpanKontrak(context),
                  bgCol: GenColor.red, col: Colors.white, pad: 15)
            ],
          )
        ],
      ),
    );

    return Container(
        padding: EdgeInsets.only(left: 30, right: 30),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
//          Text(
//            "Input Contract",
//            style: TextStyle(fontSize: 30, fontWeight: FontWeight.w600),
//          ),
          SizedBox(
            height: 20,
          ),
          Expanded(
              child: Stepper(
                  currentStep: stepIndex,
//              onStepContinue: next,
//              onStepCancel: cancel,
                  type: orientation == Orientation.landscape
                      ? StepperType.horizontal
                      : StepperType.vertical,
                  controlsBuilder: (BuildContext context,
                          {VoidCallback onStepContinue,
                          VoidCallback onStepCancel}) =>
                      Container(),
                  steps: [
                Step(
                    title: Text("Data Pelanggan"),
                    subtitle: Text("Input Data Pelanggan"),
                    content: dataPelanggan,
                    isActive: stepIndex > 0 ? true : false,
                    state:
                        stepIndex > 0 ? StepState.complete : StepState.indexed),
                Step(
                    title: Text("jangka Waktu"),
                    subtitle: Text("Input Data Pelanggan"),
                    content: jangkaWaktu,
                    isActive: stepIndex > 1 ? true : false,
                    state:
                        stepIndex > 1 ? StepState.complete : StepState.indexed),
                Step(
                    title: Text("Data Layanan"),
                    subtitle: Text("Input Data Pelanggan"),
                    content: dataLayanan,
                    isActive: stepIndex > 3 ? true : false,
                    state:
                        stepIndex > 3 ? StepState.editing : StepState.indexed)
              ]))
        ]));
  }

  void showSimpleCustomDialog(BuildContext context) {
    Dialog simpleDialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: DialogLayanan(),
    );
    showDialog(
        context: context, builder: (BuildContext context) => simpleDialog);
  }
}
