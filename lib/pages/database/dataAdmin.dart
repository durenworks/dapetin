import 'dart:convert';

import 'package:dapetinapp/api/request.dart';
import 'package:dapetinapp/common/GenColor.dart';
import 'package:dapetinapp/common/GenDimen.dart';
import 'package:dapetinapp/component/element/GenButton.dart';
import 'package:dapetinapp/component/element/genPreferrence.dart';
import 'package:dapetinapp/component/element/genShimmer.dart';
import 'package:dapetinapp/models/User.dart';
import 'package:flutter/material.dart';
import 'package:dapetinapp/blocs/baseBloc.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class DataAdmin extends StatefulWidget {
  final TabController tabController;

  DataAdmin(this.tabController);

  @override
  _DataAdminState createState() => _DataAdminState();
}

class _DataAdminState extends State<DataAdmin> {
  var bloc;
  final _streamKey = GlobalKey<FormState>();

  void gotoInputAm() {
    widget.tabController.animateTo(8);
  }

  void gotoEditAm() {
    widget.tabController.animateTo(8);
  }

  void _confirmDeleteDialog(id, name) {
    // flutter defined function
    Widget cancelButton = FlatButton(
      child: Text("Batal"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Lanjut"),
      onPressed: () {
        _delete(context, id);
        Navigator.pop(context);
      },
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Konfirmasi"),
          content: new Text("Apakah anda yakin akan menghapus " + name + "?"),
          actions: [
            cancelButton,
            continueButton,
          ],
        );
      },
    );
  }

  Future<void> _delete(context, id) async {
    if (await postDeleteUser(id)) {
      //print("sukses");
      Toast.show("Hapus Berhasil!", context, duration: Toast.LENGTH_LONG);
      (context as Element).reassemble();
    } else {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String errorMsg = prefs.getString('error_msg');
      Toast.show("Hapus Gagal : " + errorMsg, context,
          duration: Toast.LENGTH_LONG);
    }
  }

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<BaseBloc>(context);
    var media = MediaQuery.of(context);
    var size = media.size;
    bloc.am.getListAdmin();
    //setPrefferenceUserObj(null);

    var data = StreamBuilder(
        key: _streamKey,
        stream: bloc.am.listAdmin,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapShot) {
          if (!snapShot.hasData) {
            return Expanded(
              child: Container(
                child: GenShimmer(Container(
                  padding: EdgeInsets.only(left: 10, top: 38, right: 30),
                  margin: EdgeInsets.only(top: 38),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white),
                )),
              ),
            );
          } else {
            //print("snapshotnya" + snapShot.data.toString());
            return Expanded(
                child: Container(
              padding: EdgeInsets.all(GenDimen.paddingSub),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5), color: Colors.white),
//              margin: EdgeInsets.only(top: 38),
              child: ListView(children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: DataTable(
                      columnSpacing: 15,
                      sortColumnIndex: 1,
                      sortAscending: true,
                      columns: [
                        DataColumn(
                          //onSort: (columnIndex, ascending) => true,
                          label: Text('NIK'),
                        ),
                        DataColumn(label: Text('Nama')),
                        DataColumn(label: Text('Email')),
                        DataColumn(
                          label: Text('Telepon'),
                        ),
                        DataColumn(label: Text('Action')),
                        DataColumn(label: Text('')),
                      ],
                      rows: snapShot.data.map<DataRow>((e) {
                        return DataRow(cells: [
                          //                        DataCell(Text((n++).toString())),
                         
                          DataCell(Text(e["nik"] != null ? e["nik"] : "")),
                          DataCell(Text(e["name"])),
                          DataCell(Text(e["email"])),
                          DataCell(Text(e["phone"])),
                          DataCell(Row(children: <Widget>[
                            IconButton(
                              icon: Icon(Icons.edit),
                              tooltip: 'Edit',
                              onPressed: () async {
                                User user = new User(
                                              id: e["id"], 
                                              segment: e["segment"] == null
                                                        ? ""
                                                        : e["segment"]["name"].toString(),
                                              nik: e["nik"],
                                              name: e["email"],
                                              email: e["name"],
                                              phone: e["phone"],
                                              telegramUserName: e["telegram_name"],
                                              telegramId: e["telegram_id"],
                                              level: e["level"],
                                              segmentId: e["segment_id"],
                                              witel: e["witel"]
                                              ); 
                                setPrefferenceUserObj(jsonEncode(user));
                                gotoInputAm();
                              },
                            ),
                            Text(''),
                          ])),
                          DataCell(Row(children: <Widget>[
                            IconButton(
                              icon: Icon(Icons.delete),
                              tooltip: 'Delete',
                              onPressed: () {
                                _confirmDeleteDialog(e["id"], e["name"]);
                              },
                            ),
                            Text(''),
                          ])),
                        ]);
                      }).toList()),
                ),
              ]),
            ));
          }
        });

    return Container(
      padding: EdgeInsets.all(GenDimen.paddingUtama),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Data Admin",
                style: TextStyle(fontSize: GenDimen.fontTitle),
              ),
              GenButton(
                "Tambah Data ADMIN",
                () => gotoInputAm(),
                col: Colors.white,
                bgCol: GenColor.red,
                pad: 15,
                icon: Icons.add_circle,
              ),
            ],
          ),
          SizedBox(
            height: GenDimen.paddingSub,
          ),
          data
        ],
      ),
    );
  }
}
