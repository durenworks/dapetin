import 'package:dapetinapp/common/GenColor.dart';
import 'package:dapetinapp/common/GenDimen.dart';
import 'package:dapetinapp/component/element/GenButton.dart';
import 'package:flutter/material.dart';

class DataLayanan extends StatefulWidget {
  final TabController tabController;

  DataLayanan(this.tabController);

  @override
  _DataLayananState createState() => _DataLayananState();
}

class _DataLayananState extends State<DataLayanan> {
  void gotoInputLayanan() {
    widget.tabController.animateTo(10);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(GenDimen.paddingUtama),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Data Layanan",
                  style: TextStyle(fontSize: GenDimen.fontTitle),
                ),
                GenButton(
                  "Tambah Data Layanan",
                  () => gotoInputLayanan(),
                  col: Colors.white,
                  bgCol: GenColor.red,
                  pad: 15,
                  icon: Icons.add_circle,
                ),
              ],
            ),
            SizedBox(
              height: GenDimen.paddingSub,
            ),
            Expanded(
                child: Container(
              padding: EdgeInsets.all(GenDimen.paddingSub),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5), color: Colors.white),
              child: ListView(children: [
                DataTable(
                  columnSpacing: 20,
                  columns: [
                    DataColumn(label: Text("No")),
                    DataColumn(label: Text("NIK")),
                    DataColumn(label: Text("JENIS LAYANAN")),
                    DataColumn(label: Text("LAYANAN")),
                    DataColumn(label: Text("PAKET BANDWIDTH")),
                    DataColumn(label: Text("BIAYA INSTALASI")),
                    DataColumn(label: Text("BIAYA BULANAN")),
                    DataColumn(label: Text("KETERANGAN")),
                    DataColumn(label: Text("ACTION")),
                  ],
                  rows: [
                    DataRow(cells: [
                      DataCell(Text("1")),
                      DataCell(Text("1")),
                      DataCell(Text("1")),
                      DataCell(Text("1")),
                      DataCell(Text("1")),
                      DataCell(Text("1")),
                      DataCell(Text("1")),
                      DataCell(Text("1")),
                      DataCell(Text("1")),
                    ])
                  ],
                ),
              ]),
            ))
          ],
        ));
  }
}
