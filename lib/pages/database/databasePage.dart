import 'package:dapetinapp/component/element/GenButton.dart';
import 'package:flutter/material.dart';

class DatabasePage extends StatefulWidget {
  final TabController tabController;

  DatabasePage(this.tabController);

  @override
  _DatabasePageState createState() => _DatabasePageState();
}

class _DatabasePageState extends State<DatabasePage> {

  void gotoDataAm() {
    widget.tabController.animateTo(6);
  }
  void gotoDataAdmin() {
    widget.tabController.animateTo(8);
  }

  void goto(int to) {
    widget.tabController.animateTo(to);
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      child: Column(mainAxisAlignment: MainAxisAlignment.center,crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //GenButton("data layanan",()=>goto(10)),
          GenButton("Data Admin",()=>goto(7)),
          GenButton("Data AM",()=>goto(6)),
          GenButton("Data Manajer",()=>goto(11)),

        ],
      ),
    );

  }
}
