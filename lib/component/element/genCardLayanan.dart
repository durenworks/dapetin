import 'package:dapetinapp/common/GenColor.dart';
import 'package:dapetinapp/common/GenDimen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GenCardLayanan extends StatelessWidget {
  final String sid;
  final String jenisLayanan;
  final String layanan;
  final String paket;
  final String paketBandwidth;
  final String biayaInstalasi;
  final String biayaBulanan;
  final String alamatInstalasi;
  final String keterangan;
  final Function delete;

  GenCardLayanan(
      this.sid,
      this.jenisLayanan,
      this.layanan,
      this.paket,
      this.paketBandwidth,
      this.biayaInstalasi,
      this.biayaBulanan,
      this.alamatInstalasi,
      this.keterangan, {this.delete});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(GenDimen.paddingSub),
      margin: EdgeInsets.only(bottom: 20),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10), color: GenColor.input),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                    Text(
                      "SID: ",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: Colors.black),
                    ),
                    Text(
                      sid != null ? sid : "-",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: Colors.black54),
                    ),
                  ])),
              InkWell(
                onTap: delete,
                child: Icon(
                  Icons.cancel,
                  color: GenColor.red,
                ),
              ),
            ],
          ),
          SizedBox(height: 5,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "JENIS LAYANAN: ",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, color: Colors.black),
                        ),
                        Text(
                          jenisLayanan != null ? jenisLayanan : "-",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, color: Colors.black54),
                        ),
                      ])),
              Expanded(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "LAYANAN: ",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, color: Colors.black),
                        ),
                        Text(
                          layanan != null ? layanan : "-",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, color: Colors.black54),
                        ),
                      ])),
            ],
          ),
          SizedBox(height: 5,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                    Text(
                      "PAKET: ",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: Colors.black),
                    ),
                    Text(
                      paket != null ? paket : "-",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: Colors.black54),
                    ),
                  ])),
              Expanded(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                    Text(
                      "PAKET BANDWITH: ",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: Colors.black),
                    ),
                    Text(
                      paketBandwidth != null ? paketBandwidth : "-",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: Colors.black54),
                    ),
                  ])),
            ],
          ),
          SizedBox(height: 5,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                    Text(
                      "BIAYA INSTALASI: ",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: Colors.black),
                    ),
                    Text(
                      biayaInstalasi != null ? biayaInstalasi : "-",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: Colors.black54),
                    ),
                  ])),
              Expanded(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                    Text(
                      "BIAYA BULANAN: ",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: Colors.black),
                    ),
                    Text(
                      biayaBulanan != null ? biayaBulanan : "-",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: Colors.black54),
                    ),
                  ])),
            ],
          ),
          SizedBox(height: 5,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                    Text(
                      "ALAMAT INSTALASI: ",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: Colors.black),
                    ),
                    Text(
                      alamatInstalasi != null ? alamatInstalasi : "-",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: Colors.black54),
                    ),
                  ])),
              Expanded(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                    Text(
                      "KETERANGAN: ",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: Colors.black),
                    ),
                    Text(
                      keterangan != null ? keterangan : "-",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: Colors.black54),
                    ),
                  ])),
            ],
          ),
        ],
      ),
    );
  }
}
