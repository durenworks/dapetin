import 'package:dapetinapp/common/GenColor.dart';
import 'package:flutter/material.dart';

class GenDropDownMap extends StatelessWidget {
  final String label;
  final List listData;
  final Function onChange;
  final String nilai;
  final String id;
  final String val;
  GenDropDownMap(this.label, this.listData, this.nilai, this.onChange,
      {this.id, this.val});


  List data= [{"id":1,"val":"1"}, {"id":2,"val":"2"}, {"id":3,"val":"3"}];

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(label),
      SizedBox(
        height: 5,
      ),
      Container(
        padding: EdgeInsets.only(left: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: GenColor.input,
            border: Border.all(width: 1, color: GenColor.border)),
        child: DropdownButton<String>(
          onChanged: onChange,
          isExpanded: true,
          icon: Icon(Icons.arrow_drop_down),
          iconSize: 24,
          elevation: 16,
          style: TextStyle(color: Colors.deepPurple),
          underline: Container(),
          value: nilai,
          items: listData.map((item) {
            return DropdownMenuItem<String>(
              value: item[id].toString(),
              child: Text(item[val]),
            );
          }).toList(),
        ),
      ),
    ]);
  }
}
