import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

//TOKEN
Future setPrefferenceToken(String value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString('token', value);
}

Future getPrefferenceToken() async {
  String token;
  SharedPreferences prefs = await SharedPreferences.getInstance();
  token = prefs.getString('token');
  return token;
}

Future setPrefferenceErrorMsg(String value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString('error_msg', value);
}

Future getPrefferenceErrorMsg() async {
  String errMsg;
  SharedPreferences prefs = await SharedPreferences.getInstance();
  errMsg = prefs.getString('error_msg');
  return errMsg;
}

Future setPrefferenceEditId(String value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString('edit_id', value);
}

Future getPrefferenceEditId() async {
  String editId;
  SharedPreferences prefs = await SharedPreferences.getInstance();
  editId = prefs.getString('edit_id');
  return editId;
}

Future setPrefferenceUserObj(String value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString('userObj', value);
}

Future<String> getPrefferenceUserObj() async {
  String userObj;
  SharedPreferences prefs = await SharedPreferences.getInstance();
  userObj = prefs.getString('userObj');
  return userObj;
}

Future delPrefferenceUserObj() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.remove('userObj');
}

Future setPrefferenceEditKontrak(value) async{
  SharedPreferences preferences = await SharedPreferences.getInstance();
  await preferences.setInt('id_edit_kontrak', value);
}

Future getPrefferenceEditKontrak() async {
  int id;
  SharedPreferences prefs = await SharedPreferences.getInstance();
  id = prefs.getInt('id_edit_kontrak');
  return id;
}

Future delPrefferenceEditKontrak() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.remove('id_edit_kontrak');
}
