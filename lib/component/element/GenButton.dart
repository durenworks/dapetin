import 'package:flutter/material.dart';

class GenButton extends StatelessWidget {
  final String text;
  final Function onPress;
  final double pad;
  final Color bgCol;
  final Color col;
  final IconData icon;

  GenButton(this.text, this.onPress,
      {this.pad, this.bgCol, this.col, this.icon});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: onPress,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: bgCol != null ? bgCol : Colors.white),
        padding: EdgeInsets.all(pad != null ? pad : 10),
        child: icon != null
            ? Row(children: [
                Icon(
                  icon,
                  color: col != null ? col : Colors.black54,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  text,
                  style: TextStyle(color: col != null ? col : Colors.black54),
                ),
              ])
            : Text(
                text,
                style: TextStyle(color: col != null ? col : Colors.black54),
              ),
      ),
    );
  }
}
