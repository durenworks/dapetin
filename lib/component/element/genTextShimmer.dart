import 'package:dapetinapp/common/GenColor.dart';
import 'package:dapetinapp/component/element/genShimmer.dart';
import 'package:flutter/material.dart';

class GenTextShimmer extends StatelessWidget {
  final String label;
  final Function onSaved;
  final Function validator;
  final Function onTap;

  GenTextShimmer(this.label,{this.onSaved,this.validator, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(label),
      SizedBox(height: 5),
      TextFormField(
          onSaved: onSaved,
          validator: validator,
          onTap: onTap,

          decoration: InputDecoration(
            prefix: SizedBox(width: 10,),
              fillColor: Colors.black12,
              border: new OutlineInputBorder(
                borderSide: BorderSide(color: GenColor.border, width: 1),
                borderRadius: new BorderRadius.circular(5.0),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: new BorderRadius.circular(5.0),
                borderSide: BorderSide(color: GenColor.border, width: 1.0),
              ),
              focusedBorder: new OutlineInputBorder(
                borderSide: BorderSide(color: GenColor.hoverGreen, width: 1),
                borderRadius: new BorderRadius.circular(5.0),
              ),

              contentPadding: EdgeInsets.all(5),
              filled: true),
        ),
    ]);
  }
}
