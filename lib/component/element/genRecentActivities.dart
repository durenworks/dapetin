import 'package:dapetinapp/common/GenColor.dart';
import 'package:flutter/material.dart';

class GenRecentActivities extends StatelessWidget {
  final String status;
  final String noContract;
  final String tanggal;

  GenRecentActivities(
    this.status,
    this.noContract,
    this.tanggal,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: status == "Amandement"
                    ? GenColor.hoverGreen
                    : GenColor.hoverRed),
            width: 50,
            height: 50,
            child: Icon(Icons.assignment,
                color: status == "Amandement"
                    ? Colors.greenAccent
                    : Colors.redAccent),
          ),
          SizedBox(
            width: 10,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                status,
                style: TextStyle(fontWeight: FontWeight.w300, fontSize: 16),
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                noContract,
                style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                tanggal,
                style: TextStyle(fontWeight: FontWeight.w300, fontSize: 12),
              ),
            ],
          )
        ],
      ),
    );
  }
}
