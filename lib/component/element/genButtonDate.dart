import 'package:dapetinapp/common/GenColor.dart';
import 'package:flutter/material.dart';

class GenButtonDate extends StatelessWidget {
  final String label;
  final Function takeDate;
  final String textDate;

  GenButtonDate(this.label, this.takeDate, this.textDate);


  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label),
          SizedBox(height: 5),
          FlatButton(
            onPressed: takeDate,
            color: GenColor.input,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
                side: BorderSide(width: 1, color: GenColor.border)),
            child: Container(
              padding: EdgeInsets.only(top: 15, bottom: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Text(textDate == null ? "PILIH TANGGAL" : textDate), Icon(Icons.date_range)],
              ),
            ),
          )
        ],
      ),
    );
  }
}
