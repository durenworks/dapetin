import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class GenShimmer extends StatelessWidget {
  final Widget _child;

  const GenShimmer(this._child);
  @override
  Widget build(BuildContext context) {
    return Container(color: Color.fromARGB(100, 0, 0, 0));
  }
}
