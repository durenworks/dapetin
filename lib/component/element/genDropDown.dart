import 'package:dapetinapp/common/GenColor.dart';
import 'package:flutter/material.dart';

class GenDropDown extends StatelessWidget {
  final String label;
  final List<String> listData;
  final Function onChange;
  final String nilai;
  final TextEditingController controller;
  GenDropDown(this.label, this.listData,  this.nilai, this.onChange, {this.controller});

  @override
  Widget build(BuildContext context) {
    return Column( crossAxisAlignment: CrossAxisAlignment.start,children: [
      Text(label),
      SizedBox(
        height: 5,
      ),
      Container(
        padding: EdgeInsets.only(left: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10), color: GenColor.input, border: Border.all(width: 1, color: GenColor.border)),
        child: DropdownButton<String>(
          onChanged: onChange,
          isExpanded: true,
          icon: Icon(Icons.arrow_drop_down),
          iconSize: 24,
          elevation: 16,
          style: TextStyle(color: Colors.deepPurple),
          underline: Container(),
          value: nilai,
          items: listData.map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
      ),
    ]);
  }
}
