import 'package:dapetinapp/localdb/db.dart';
import 'package:dapetinapp/models/layanan.dart';
import 'package:sembast/sembast.dart';

class DBLogic {
  static const String STORE = 'notes';

  final _notes = intMapStoreFactory.store(STORE);

  Future<Database> get db async => await DapetinDB.instance.db;

  Future insert(Layanan note) async {
    await _notes.add(await db, note.toMap());
  }

  Future update(Layanan note) async {
    final finder = Finder(filter: Filter.byKey(note.id));

    await _notes.update(
      await db,
      note.toMap(),
      finder: finder,
    );
  }

  Future delete(Layanan note) async {
    final finder = Finder(filter: Filter.byKey(note.id));

    await _notes.delete(
      await db,
      finder: finder,
    );
  }

  Future deleteAll() async {
    await _notes.delete(
      await db,
    );
  }

  Future<List<Layanan>> getAllNotes() async {
    final snapshot = await _notes.find(await db);

    return snapshot.map((map) {
      final note = Layanan.fromMap(map.value);

      note.id = map.key;
      return note;
    }).toList();
  }
}
