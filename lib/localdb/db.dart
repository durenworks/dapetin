import 'package:sembast/sembast.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

import 'dart:async';

import 'package:sembast_web/sembast_web.dart';

class DapetinDB {
  DapetinDB._internal();

  static DapetinDB get instance => _singleton;

  static final DapetinDB _singleton = DapetinDB._internal();

  static Database _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }

    _db = await initDB();

    return _db;
  }

  Future initDB() async {
    var factory = databaseFactoryWeb;

    // Open the database
    var db = await factory.openDatabase('dapetinWebDB');
    return db;
  }
}
