import 'package:flutter/material.dart';

class GenDimen {
  static const double fontTitle = 30;
  static const double fontSubTitle = 20;
  static const double jarakTextField = 20;
  static const double paddingUtama = 30;
  static const double paddingSub = 20;
  static const double paddingButtonBesar = 20;
}
