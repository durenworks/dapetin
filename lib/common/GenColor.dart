import 'package:flutter/material.dart';

class GenColor {
  static const grey = Color.fromARGB(153, 153, 153, 1);
  static const lightGrey = Color.fromRGBO(216, 216, 216, 0.2);
  static const blue = Color.fromRGBO(46, 91, 255, 1);
  static const orange = Color.fromRGBO(247, 104, 91, 1);
  static const red = Color.fromRGBO(212, 0, 0, 1);
  static const hoverRed = Color.fromARGB(120, 255, 230, 230);
  static const green = Color.fromRGBO(71, 228, 194, 1);
  static const hoverGreen = Color.fromRGBO(71, 228, 194, 0.1);
  static const border = Color.fromARGB(50,0, 0, 0);
  static const input = Color.fromARGB(255, 249,250,255);
  static const redButton = Color.fromARGB(255,212,0,0);
}
