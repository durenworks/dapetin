import 'dart:async';

import 'package:dapetinapp/api/request.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';
import 'package:rxdart/rxdart.dart';

class AMBloc extends ChangeNotifier {
  final req = new GenRequest();
  final _listAM = PublishSubject();
  final _listAdmin = PublishSubject();
  final _listResponse = PublishSubject();

  Stream get listAm => _listAM.asBroadcastStream();
  Stream get listAdmin => _listAdmin.asBroadcastStream();

  getListAm() async {
    dynamic data = await req.postSearchUser("", "ACCOUNT MANAGER", "ALL");
    if (data['status'] == "success") {
      _listAM.add(data['data']);
    }
  }

  getListAdmin() async {
    dynamic data = await req.postSearchUser("", "SUPERADMIN", "ALL");
    if (data['status'] == "success") {
      _listAdmin.add(data['data']);
    }
  }

  getListManajer() async {
    dynamic data = await req.postSearchUser("", "MANAGER", "ALL");
    if (data['status'] == "success") {
      _listAM.add(data['data']);
    }
  }

  getUserDetail(id) async {
    dynamic data = await req.postGetUserDetail(id);
    if (data['status'] == "success") {
      _listAM.add(data['data']);
    }
  }

  postAddAm(String segment, String nik, String name, String email,
  String telegramId, String phone, String telegramUsername, String witel, String password,
    String level) async {
    dynamic data = await req.postAddUser(segment, nik, name, email,
   telegramId,  phone,  telegramUsername,  witel,  password, level);
    _listResponse.add(data['data']);
  }
}
