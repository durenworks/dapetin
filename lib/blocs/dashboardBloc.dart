import 'dart:async';

import 'package:dapetinapp/api/request.dart';
import 'package:dapetinapp/component/element/genPreferrence.dart';
import 'package:dapetinapp/models/Kontrak.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';
import 'package:rxdart/rxdart.dart';

class DashboardBloc extends ChangeNotifier {
  final req = new GenRequest();


  final _statistics = PublishSubject();
  final _kontrakReminder = PublishSubject();
  final _listRecentActivities = PublishSubject();
  final _listReminder = PublishSubject();
  final _listSegment = PublishSubject();
  final _listAm = PublishSubject();
  final _listServiceType = PublishSubject();
  final _listService = PublishSubject();
  final _dataKontrak = PublishSubject();

  Stream get statistics => _statistics.asBroadcastStream();
  Stream get kontrakReminder => _kontrakReminder.asBroadcastStream();
  Stream get listRecentActivities => _listRecentActivities.asBroadcastStream();
  Stream get listReminder => _listReminder.asBroadcastStream();
  Stream get listSegment => _listSegment.asBroadcastStream();
  Stream get listAm => _listAm.asBroadcastStream();
  Stream get listServiceType => _listServiceType.asBroadcastStream();
  Stream get listService => _listService.asBroadcastStream();
  Stream get dataKontrak => _dataKontrak.asBroadcastStream();

  var nama_pelanggan;

  getStatistics() async {
    dynamic data =
    await req.getApi("/statistics");
    if (data['status'] == "success") {
      _statistics.add(data['data']);
    }
  }

  getListReminders(String token) async {
    dynamic data = await req.postApi("/contract/reminder", {'token':token});
    if (data['status'] == "success") {
      _listReminder.add(data);
    }
  }

  getListRecentActivities() async {
    dynamic data = await req.getApi("/contract/recent_activities");
    if (data['status'] == "success") {
      _listRecentActivities.add(data);
    }
  }

  getListAm() async {
    dynamic data = await req.getApi("/am");
    if (data['status'] == "success") {
      _listAm.add(data['data']);
    }
  }

  getListServiceType() async {
    dynamic data = await req.getApi("/service_type");
    if (data['status'] == "success") {
      _listServiceType.add(data['data']);
    }
  }

  getListService(String serviceType) async {
    dynamic data = await req.getApi("/service/"+serviceType);
    if (data['status'] == "success") {
      _listService.add(data['data']);
    }
  }

  getListListSegment() async {
    dynamic data = await req.getApi("/segment");
    //print(data);
    //print('foo');
    if (data['status'] == "success") {
      _listSegment.add(data['data']);
//      data[].map((i)=>Photo.fromJson(i)).toList();
    }
  }

  getDataKontrak(String token, int id) async {
    dynamic data = await req.postApi("/contract/detail", {
      'token': "y7kx2poZOuOzAfgzdNFTSUg6ZO0WFwSqqedJmkjtSRSRQ2Sz92Bvjg603AsRi3Sq4MHUPpnsvnGG3zXh",
      "id": id
    });
    return data["status"];
  }

}
