import 'dart:async';

import 'package:dapetinapp/localdb/crud.dart';
import 'package:dapetinapp/models/layanan.dart';
import 'package:rxdart/rxdart.dart';


class NoteBloc {
  final DBLogic logic;

  StreamController<List<Layanan>> _notes = StreamController.broadcast();

  StreamController<Layanan> _incoming = StreamController();

  Stream<List<Layanan>> get outgoing => _notes.stream;

  Stream get listLayanan => _notes.stream;

  StreamSink<Layanan> get inSink => _incoming.sink;

  NoteBloc(this.logic) {
    _incoming.stream.listen((note) async {
      switch (note.state) {
        case NotesState.INSERT:
          logic.insert(note).then((_) async => {
                _notes.add(await logic.getAllNotes()),
              });
          break;
        case NotesState.UPDATE:
          logic.update(note).then((_) async => {
                _notes.add(await logic.getAllNotes()),
              });
          break;
        case NotesState.GETALL:
          _notes.add(await logic.getAllNotes());
          break;
        case NotesState.DETLETE:
          logic.delete(note).then((_) async => {
                _notes.add(await logic.getAllNotes()),
              });
          break;
        case NotesState.DELETE_ALL:
          logic.deleteAll().then((_) async => {
                _notes.add(await logic.getAllNotes()),
              });
          break;
        case NotesState.NOOP:
          break;
      }
    });
  }

  void dispose() {
    _incoming.close();
    _notes.close();
  }
}
