import 'dart:convert';

class User {
  int id;
  String segment;
  String nik;
  String name;
  String email;
  String phone;
  String witel;
  String telegramId;
  String telegramUserName;
  String level;
  int segmentId;

  User({this.id = 0, this.segment, this.nik, this.name, this.email, this.phone, this.witel,
        this.telegramId, this.telegramUserName, this.level, this.segmentId});

  factory User.fromJson(Map<String, dynamic> map) {
    return User(
        id: map["id"],
        segment: map["segment"] == null ? "" : map["segment"]["acronym"], 
        nik: map["nik"], 
        name: map["name"], 
        email: map["email"], 
        phone: map["phone"],
        witel: map["witel"],
        telegramId: map["telegram_id"],
        telegramUserName: map["telegram_name"],
        level: map["level"],
        segmentId: map["segment_id"]
        );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "segment": segment,
      "nik": nik,
      "name": name, 
      "email": email, 
      "phone": phone,
      "witel": witel,
      "telegram_id": telegramId,
      "telegram_name": telegramUserName,
      "level": level,
      "segment_id": segmentId
    };
  }

  static User userFromJson() {}

  // @override
  // String toString() {
  //   return 'Profile{id: $id, name: $name, email: $email, age: $age}';
  // }
}

List<User> userFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<User>.from(data.map((item) => User.fromJson(item)));
}

String userToJson(User data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}