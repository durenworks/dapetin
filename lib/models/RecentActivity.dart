import 'dart:convert';

class RecentActivity {
  String contractNumber;
  String activities;

  var data;

  RecentActivity({this.contractNumber, this.activities});

  factory RecentActivity.fromJson(Map<String, dynamic> map) {
    return RecentActivity(
        contractNumber: map["contract_number"], activities: map["activities"]);
  }

  Map<String, dynamic> toJson() {
    return {"contractNumber": contractNumber, "activities": activities};
  }

  @override
  String toString() {
    return 'RecentActivity{id: $contractNumber, activities: $activities}';
  }

}

List<RecentActivity> recentActivityFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<RecentActivity>.from(data.map((item) => RecentActivity.fromJson(item)));
}

String recentActivityToJson(RecentActivity data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}