import 'dart:convert';

class Kontrak {
  int id;
  //DATA PELANGGAN
  String noKontrak;
  String segment;
  String am;
  String namaPelanggan;
  String npwp;
  String alamatPelanggan;

  //ttd
  String namaPic1;
  String jabatan1;
  String namaPic2;
  String jabatan2;
  String emailpic1;
  String emailpic2;

  //JANGKA WAKTU
  String startDate;
  String endDate;
  String masaBerlaku;


  Kontrak({this.id = 0, this.segment, this.am, this.namaPelanggan, this.npwp, this.alamatPelanggan, this.namaPic1,
        this.jabatan1, this.namaPic2, this.jabatan2, this.emailpic1, this.emailpic2,this.startDate, this.endDate, this.masaBerlaku });

  factory Kontrak.fromJson(Map<String, dynamic> map) {
    return Kontrak(
        id: map["id"],
        segment: map["segment_id"],
        am: map["user_id"],
        namaPelanggan: map["customer_name"],
        npwp: map["customer_npwp"],
        alamatPelanggan: map["customer_address"],
        namaPic1: map["customer_pic1_name"],
        jabatan1: map["customer_pic1_position"],
        namaPic2: map["customer_pic2_name"],
        jabatan2: map["customer_pic2_position"],
        emailpic1: map["customer_pic1_email"],
        emailpic2: map["customer_pic2_email"],
        startDate: map["start_date"],
        endDate: map["end_date"],
        masaBerlaku: map["duration"]);
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "segment_id": segment,
      "user_id": am,
      "customer_name": namaPelanggan,
      "customer_npwp": npwp,
      "customer_address": alamatPelanggan,
      "customer_pic1_name": namaPic1,
      "customer_pic1_position": jabatan1,
      "customer_pic2_name": namaPic2,
      "customer_pic2_position": jabatan2,
      "customer_pic1_email": emailpic1,
      "customer_pic2_email": emailpic2,
      "start_date": startDate,
      "end_date": endDate,
      "duration": masaBerlaku,

    };
  }

  static Kontrak userFromJson() {}

  // @override
  // String toString() {
  //   return 'Profile{id: $id, name: $name, email: $email, age: $age}';
  // }
}

List<Kontrak> userFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<Kontrak>.from(data.map((item) => Kontrak.fromJson(item)));
}

String userToJson(Kontrak data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}