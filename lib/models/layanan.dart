enum NotesState {
  GETALL,
  INSERT,
  UPDATE,
  DETLETE,
  DELETE_ALL,
  NOOP,
}

class Layanan {
  int id;

  NotesState state;
  bool editing;

   String noKontrak;
   String sid;
   String jenisLayanan;
   String layanan;
   String paket;
   String paketBandwith;
   String alamatInstalasi;
   String biayaInstalasi;
   String biayaBulanan;
   String keterangan;

  Layanan({
    this.noKontrak,
    this.sid,
    this.jenisLayanan,
    this.layanan,
    this.paket,
    this.paketBandwith,
    this.alamatInstalasi,
    this.biayaInstalasi,
    this.biayaBulanan,
    this.keterangan,
    this.editing = false,
    this.state,
    this.id,
  });

  Layanan copyWith({
    String noKontrak,
    String sid,
    String jenisLayanan,
    String layanan,
    String paket,
    String paketBandwith,
    String alamatInstalasi,
    String biayaInstalasi,
    String biayaBulanan,
    String keterangan,
    NotesState state,
    int id,
    bool editing,
  }) {
    return Layanan(
      noKontrak: noKontrak ?? this.noKontrak,
      sid: sid ?? this.sid,
      jenisLayanan: jenisLayanan ?? this.jenisLayanan,
      layanan: layanan ?? this.layanan,
      paket: sid ?? this.paket,
      paketBandwith: sid ?? this.paketBandwith,
      alamatInstalasi: sid ?? this.alamatInstalasi,
      biayaInstalasi: sid ?? this.biayaInstalasi,
      biayaBulanan: sid ?? this.biayaBulanan,
      keterangan: sid ?? this.keterangan,
      state: state ?? this.state,
      editing: editing ?? this.editing,
      id: id ?? this.id,
    );
  }

  Layanan.fromMap(Map<String, dynamic> map)
      : this.noKontrak = map['noKontrak'],
        this.sid = map['sid'],
        this.jenisLayanan = map['jenisLayanan'],
        this.layanan = map['layanan'],
        this.paket = map['paket'],
        this.paketBandwith = map['paketBandwith'],
        this.alamatInstalasi = map['alamatInstalasi'],
        this.biayaInstalasi = map['biayaInstalasi'],
        this.biayaBulanan = map['biayaBulanan'],
        this.keterangan = map['keterangan'],
        this.editing = map['editing'],
        this.state = NotesState.values[map['state']];

  Map<String, dynamic> toMap() {
    return {
      'noKontrak': noKontrak,
      'sid': sid,
      'jenisLayanan': jenisLayanan,
      'layanan': layanan,
      'paket': paket,
      'paketBandwith': paketBandwith,
      'alamatInstalasi': alamatInstalasi,
      'biayaInstalasi': biayaInstalasi,
      'biayaBulanan': biayaBulanan,
      'keterangan': keterangan,
      'editing': editing,
      'state': state.index,
    };
  }
}
