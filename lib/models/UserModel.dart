import 'dart:convert';

class UserModel {
  String username;
  String password;

  var data;

  UserModel({this.username, this.password});

  factory UserModel.fromJson(Map<String, dynamic> map) {
    return UserModel(
        username: map["username"], password: map["password"]);
  }

  Map<String, dynamic> toJson() {
    return {"username": username, "password": password};
  }

  @override
  String toString() {
    return 'UserModel{id: $username, activities: $password}';
  }


}

List<UserModel> userModelFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<UserModel>.from(data.map((item) => UserModel.fromJson(item)));
}

String userModelToJson(UserModel data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}