import 'dart:convert';

import 'package:dapetinapp/component/element/genPreferrence.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

String ip = "https://dapetin.canggih.dev";
//String ip = "http://192.168.137.1:8002";
//String ip = "http://192.168.43.224:8002";
//String ip = "http://10.200.54.52:8000";
String token;

Future getToken() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  token = prefs.getString('token');
}

postGetContractDetail(token,id) async {

  try {
    String path = "$ip/contract/detail";
    String token = await getToken();
    Response response =
    await Dio().post(path, data: {"token": "y7kx2poZOuOzAfgzdNFTSUg6ZO0WFwSqqedJmkjtSRSRQ2Sz92Bvjg603AsRi3Sq4MHUPpnsvnGG3zXh", "id": id});
    print(id);
//    print(response.data['data'][0]);
    return response.data;
  } catch (e) {
    print("error code: " + e.toString());
    return false;
  }
}

Future postLogin(String email, String password) async {
  try {
    String path = "$ip/login";
    Response response =
        await Dio().post(path, data: {"email": email, "password": password});
    //print(response.data);
    if (response.data["message"] == "Login Success") {
      //print("berhasil");
      setPrefferenceToken(response.data["data"]["token"]);
      return true;
    } else {
      //print("gagal");
      return false;
    }
  } catch (e) {
    print("error code: " + e.toString());
    return false;
  }
}

postEditUser(
    int userId,
    String segment,
    String nik,
    String name,
    String email,
    String telegramId,
    String phone,
    String telegramUsername,
    String witel,
    String password,
    String level) async {
  try {
    String path = "$ip/superadmin/user/update";
    String token = await getToken();
    Object data;

    if (password == '') {
      data = {
        "token": token,
        "id": userId,
        "email": email,
        "segment": segment,
        "telegram_id": telegramId,
        "telegram_name": telegramUsername,
        "name": name,
        "phone": phone,
        "nik": nik,
        "level": level,
      };
    } else {
      data = {
        "token": token,
        "id": userId,
        "email": email,
        "segment": segment,
        "telegram_id": telegramId,
        "telegram_name": telegramUsername,
        "password": password,
        "name": name,
        "phone": phone,
        "nik": nik,
        "level": level,
      };
    }

    //print(data);

    Response response = await Dio().post(path, data: data);

    if (response.data["status"] == "success") {
      //print("berhasil");
      await setPrefferenceErrorMsg(response.data["message"]);
      return true;
    } else {
      //print("gagal");
      await setPrefferenceErrorMsg(response.data["message"]);
      return false;
    }
  } catch (e) {
    print("error code: " + e.toString());
    return false;
  }
}

postAddUser(
    String segment,
    String nik,
    String name,
    String email,
    String telegramId,
    String phone,
    String telegramUsername,
    String witel,
    String password,
    String level) async {
  try {
    String path = "$ip/superadmin/user/store";
    String token = await getToken();
    var data = {
      "token": token,
      "email": email,
      "segment": segment,
      "telegram_id": telegramId,
      "telegram_name": telegramUsername,
      "password": password,
      "name": name,
      "phone": phone,
      "nik": nik,
      "level": level,
    };

    //print(path);
    //print(data);
    Response response = await Dio().post(path, data: data);

    if (response.data["status"] == "success") {
      //print("berhasil");
      setPrefferenceErrorMsg(response.data["message"]);
      return true;
    } else {
      //print("gagal");
      setPrefferenceErrorMsg(response.data["message"]);
      return false;
    }
  } catch (e) {
    print("error code: " + e.toString());
    return false;
  }
}

postAddKontrak(
    String noKontrak,
    String segment,
    String am,
    String nama_cus,
    String npwp_cus,
    String addres_cus,
    String nama_pic1,
    String jabatan_pic1,
    String nama_pic2,
    String jabatan_pic2,
    String email_pic1,
    String email_pic2,
    String durasi,
    String start,
    String end,
    List listLayanan
    ) async {
  print("try to input");
  var param = {


      "sid": "sid123",
      "service_type_id": "1",
      "service_id": "1",
      "package": "10Mbps",
      "bandwith_package": "Upto 10Mbps",
      "instalation_fee": "1000000",
      "monthly_fee": "200000",
      "note": "this is note"

  };
  Map param2 = {


      "sid": "sid123",
      "service_type_id": "1",
      "service_id": "1",
      "package": "10Mbps",
      "bandwith_package": "Upto 10Mbps",
      "instalation_fee": "1000000",
      "monthly_fee": "200000",
      "note": "this is note"

  };
  var arrServices = [param,param2];
  try {
    String path = "$ip/contract/store";
    String token = await getToken();
    var data = {
//      "token":
//      "y7kx2poZOuOzAfgzdNFTSUg6ZO0WFwSqqedJmkjtSRSRQ2Sz92Bvjg603AsRi3Sq4MHUPpnsvnGG3zXh",
//      "contract_number": "K.TEL 004/HK.820/TR0-W500/2020",
//      "segment_id": "1",
//      "am_id": "2",
//      "customer_name": "tes",
//      "customer_npwp": "1209-09812-0981",
//      "customer_address": "jogja",
//      "customer_pic1_name": "luigi",
//      "customer_pic1_position": "Manajer",
//      "customer_pic2_name": "nirmala",
//      "customer_pic2_position": "Finance",
//      "customer_pic1_email": "luigi@mail.com",
//      "customer_pic2_email": "nirmala@mail.com",
//      "duration": "12",
//      "start_date": "2019-07-01",
//      "end_date": "2019-07-01",
//    "services[0]": jsonEncode(param),

      "token":
      "y7kx2poZOuOzAfgzdNFTSUg6ZO0WFwSqqedJmkjtSRSRQ2Sz92Bvjg603AsRi3Sq4MHUPpnsvnGG3zXh",
      "contract_number": noKontrak,
      "segment_id": segment,
      "am_id": am,
      "customer_name": nama_cus,
      "customer_npwp": npwp_cus,
      "customer_address": addres_cus,
      "customer_pic1_name": nama_pic1,
      "customer_pic1_position": jabatan_pic1,
      "customer_pic2_name": nama_pic2,
      "customer_pic2_position": jabatan_pic2,
      "customer_pic1_email": email_pic1,
      "customer_pic2_email": email_pic2,
      "duration": durasi,
      "start_date": start,
      "end_date": end,
      "services": listLayanan,
    };
    print(path);
    print(data);
    print(arrServices);
    Response response = await Dio().post(path, data: data);

    if (response.data["status"] == "success") {
      print("berhasil");
      setPrefferenceErrorMsg(response.data["message"]);
      return true;
    } else {
      print("gagal");
      setPrefferenceErrorMsg(response.data["message"]);
      return false;
    }
  } catch(e){
    return false;
  }
}

postDeleteUser(id) async {
  try {
    String path = "$ip/superadmin/user/delete";
    String token = await getToken();
    var data = {
      "token": token,
      "id": id,
    };
    Response response = await Dio().post(path, data: data);

    if (response.data["status"] == "success") {
      //print("berhasil");
      setPrefferenceToken(response.data["mesage"]);
      return true;
    } else {
      //print("gagal");
      setPrefferenceToken(response.data["mesage"]);
      return false;
    }
  } catch (e) {
    print("error code: " + e.toString());
    return false;
  }
}

//dynamic get(portal) async {
//  try {
//    await getToken();
////     token = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImRjMGMzNWZlYjBjODIzYjQyNzdkZDBhYjIwNDQzMDY5ZGYzMGZkZWEiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiZ2Vub3NzeXMiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tL2EtL0FPaDE0R2lTaGVBSndRWElMU0xaaVR1bV9XTW15eDUxejZaOU12eWM1QTZJPXM5Ni1jIiwiaXNzIjoiaHR0cHM6Ly9zZWN1cmV0b2tlbi5nb29nbGUuY29tL3NvbG9wb3MtYXBwIiwiYXVkIjoic29sb3Bvcy1hcHAiLCJhdXRoX3RpbWUiOjE1ODY3NTc3NTMsInVzZXJfaWQiOiJHNEp2RktlZWFJZElZbzh3S2x2bTUwVWV5MDIyIiwic3ViIjoiRzRKdkZLZWVhSWRJWW84d0tsdm01MFVleTAyMiIsImlhdCI6MTU4Njc1Nzc1NCwiZXhwIjoxNTg2NzYxMzU0LCJlbWFpbCI6Imdlbm9zc3lzMjAxOUBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJnb29nbGUuY29tIjpbIjExNzM5MjE0ODQ1MjY0ODQyOTAxNSJdLCJlbWFpbCI6WyJnZW5vc3N5czIwMTlAZ21haWwuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoiZ29vZ2xlLmNvbSJ9fQ.FeR4uiDwQUN-uDCbAiB7sE44-Ei-siQwKGXFhhhqSaIpvQuBqdY8yp_jHIOhKcI-ElZoWf0ailH75CK_HRplN6-MNnzaDF6maJfdjgAcOW3DxZKIZr9tUeas82XNfb0E_wi3QRcLxZZCIuH2R34Qzv0McThXHQVjEjljOpQ92JmvcHRyIp3SC4wLAzyPGAxolgOaV7913jTl0UrxN-xKnjfryb-7Kocxwfj_9YBU2KP1H_Zr2W9WMJblc4xQt98iMc-6hRp-vrQHVifw-7lqLy2L_i4in5ME28pc_cPWwCrWG6pr1ULp0EiQkuSyxCRc8DmNSHV2hKtn4J79LXg1WQ";
//    Response response = await Dio().get(ip + "/api/" + portal,
//        options: Options(headers: {'GENOSSYS-X': 'SOLOPOS ' + token}));
//    return response.data;
//  } catch (e) {
//    print("error code: " + e.toString());
//    return "error";
//  }
//}

class GenRequest {
  getApi(portal) async {
    try {
      await getToken();
      Response response = await Dio().get(ip + portal);
//      print(response.data);
      return response.data;
    } catch (e) {
      //print("error code: " + e.toString());
      return "error";
    }
  }

  postApi(portal, Map<dynamic, dynamic> data) async {
    try {
      await getToken();
      Response response = await Dio().post(ip + portal, data: data);

      return response.data;
    } catch (e) {
      print("error code: " + e.toString());

      return 500;
    }
  }

  deleteApi(portal) async {
    try {
      Response response = await Dio().delete(ip + "/api/" + portal,
          options: Options(headers: {'GENOSSYS-X': 'SOLOPOS ' + token}));
      return response.data;
    } catch (e) {
      return 500;
    }
  }

  postSearchUser(String keyword, String level, String segment) async {
    try {
      String path = "$ip/superadmin/user/";
      //String token = await getToken();
      Response response = await Dio().post(path,
          data: {"level": level, "segment": segment, "token": token});
      //print(response.data);
      return response.data;
    } catch (e) {
      print("error code: " + e.toString());
      return false;
    }
  }

  postAddUser(
      String segment,
      String nik,
      String name,
      String email,
      String telegramId,
      String phone,
      String telegramUsername,
      String witel,
      String password,
      String level) async {
    try {
      String path = "$ip/superadmin/user/store";
      String token = await getToken();
      Response response = await Dio().post(path, data: {
        "token": token,
        "email": email,
        "segment": segment,
        "telegram_id": telegramId,
        "telegram_name": telegramUsername,
        "password": password,
        "name": name,
        "phone": phone,
        "nik": nik,
        "level": level,
      });
      //print(response.data);
      return response.data;
    } catch (e) {
      print("error code: " + e.toString());
      return false;
    }
  }

  postGetUserDetail(id) async {
    try {
      String path = "$ip/superadmin/user/detail";
      String token = await getToken();
      Response response =
          await Dio().post(path, data: {"token": token, "id": id});
      //print(response.data);
      return response.data;
    } catch (e) {
      print("error code: " + e.toString());
      return false;
    }
  }
}
